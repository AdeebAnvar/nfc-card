import 'package:flutter/widgets.dart';

class SocialMediaConnections {
  String link;
  IconData icon;
  SocialMediaConnections({
    required this.link,
    required this.icon,
  });
}
