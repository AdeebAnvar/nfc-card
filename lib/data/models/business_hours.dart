// ignore_for_file: public_member_api_docs, sort_constructors_first
class BusinessHours {
  String day;
  String timeFrom;
  String timeTo;
  bool isClosed;

  BusinessHours({
    required this.day,
    this.timeFrom = "09:00 AM",
    this.timeTo = "09:00 PM",
    this.isClosed = false,
  });
}
