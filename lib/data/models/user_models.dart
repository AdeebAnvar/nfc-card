// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_application_1/data/models/company_models.dart';
import 'package:flutter_application_1/data/models/contact_details.dart';

class UserModels {
  String name;
  String designation;
  String avatar;

  String profileDescription;
  CompanyModel company;
  List<ContactDetails> contactDetails;

  UserModels({
    required this.name,
    required this.designation,
    required this.avatar,
    required this.profileDescription,
    required this.company,
    required this.contactDetails,
  });
}
