// ignore_for_file: public_member_api_docs, sort_constructors_first
class Testimonials {
  String image;
  String testimonial;
  String? label;
  Testimonials({
    required this.image,
    required this.testimonial,
    this.label,
  });
}
