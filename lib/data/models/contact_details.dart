// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/widgets.dart';

class ContactDetails {
  IconData icon;
  String label;
  String content;
  ContactDetails({
    required this.icon,
    required this.label,
    required this.content,
  });
}
