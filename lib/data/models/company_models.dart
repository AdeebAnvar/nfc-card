// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_application_1/data/models/business_hours.dart';
import 'package:flutter_application_1/data/models/social_media.dart';

class CompanyModel {
  String companyName;
  String companyLogo;
  String coverImage;
  String qrCode;
  List<SocialMediaConnections> socialMediaConnections;
  List<BusinessHours> businessHours;
  CompanyModel({
    required this.companyName,
    required this.companyLogo,
    required this.coverImage,
    required this.qrCode,
    required this.socialMediaConnections,
    required this.businessHours,
  });
}
