// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_application_1/data/models/business_hours.dart';
import 'package:flutter_application_1/data/models/company_models.dart';
import 'package:flutter_application_1/data/models/contact_details.dart';
import 'package:flutter_application_1/data/models/galleries.dart';
import 'package:flutter_application_1/data/models/products.dart';
import 'package:flutter_application_1/data/models/services.dart';
import 'package:flutter_application_1/data/models/social_media.dart';
import 'package:flutter_application_1/data/models/testimonials.dart';
import 'package:flutter_application_1/data/models/user_models.dart';

List<Testimonials> getTestimonials() => [
      Testimonials(label: 'Test Testimonial', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', testimonial: loremIpsum),
    ];

List<Products> getProducts() => [
      Products(name: 'Test Products', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', description: loremIpsum, price: '₹2500.00'),
      Products(name: 'Test Product 2', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', description: loremIpsum, price: '₹2500.00'),
      Products(name: 'Test Product 4', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', description: loremIpsum, price: '₹2500.00'),
    ];

List<Galleries> getGalleries() {
  return [
    Galleries(image: 'https://www.mye.pw/uploads/vcards/services/41/curve-shape.png'),
    Galleries(image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
    Galleries(image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
  ];
}

UserModels getUserData() {
  return UserModels(
    name: 'Akhil Sabu',
    avatar: 'https://mye.pw/web/media/avatars/150-26.jpg',
    profileDescription: 'Just dummy description',
    designation: 'CEO',
    contactDetails: getContactDetails(),
    company: getCompanyData(),
  );
}

List<ContactDetails> getContactDetails() => [
      ContactDetails(icon: Icons.email, label: 'Email', content: 'akhilsabuv@gmail.com'),
      ContactDetails(icon: Icons.email, label: 'Alternate Email', content: 'akhilsabuv@gmail.com'),
      ContactDetails(icon: Icons.phone, label: 'Mobile Number', content: '+91 7592864440'),
      ContactDetails(icon: Icons.phone, label: 'Mobile Number', content: '+91 7592864440'),
      ContactDetails(icon: Icons.cake, label: 'Date of Birth', content: '23/04/2001'),
      ContactDetails(icon: Icons.location_on_outlined, label: 'Address', content: 'Lucas Street, Imayath Line AC Thod'),
    ];

CompanyModel getCompanyData() {
  return CompanyModel(
    coverImage: 'https://mye.pw/assets/images/default_cover_image.jpg',
    companyName: 'Festa',
    companyLogo: '',
    qrCode:
        'https://imgs.search.brave.com/gW3hG6LUoOEHDu7YI9Vt_TSng96W0lL6YQekJU4mtTQ/rs:fit:860:0:0/g:ce/aHR0cHM6Ly91c2Vy/LWltYWdlcy5naXRo/dWJ1c2VyY29udGVu/dC5jb20vMjM2OTAx/NDUvODMzNDg3Mzkt/YzRmODhkMDAtYTM2/MS0xMWVhLTkzMmUt/ZTcyMmUwYmQxYjY1/LnBuZw',
    socialMediaConnections: [
      SocialMediaConnections(icon: Icons.facebook, link: ''),
      SocialMediaConnections(icon: Icons.public, link: ''),
      SocialMediaConnections(icon: Icons.supervised_user_circle_outlined, link: ''),
      SocialMediaConnections(icon: Icons.account_balance_rounded, link: ''),
    ],
    businessHours: [
      BusinessHours(day: 'Monday', timeFrom: '09:00 AM', timeTo: '09:00 PM'),
      BusinessHours(day: 'Tuesday', timeFrom: '09:00 AM', timeTo: '09:00 PM'),
      BusinessHours(day: 'Wednesday', timeFrom: '09:00 AM', timeTo: '09:00 PM'),
      BusinessHours(day: 'Thursday', timeFrom: '09:00 AM', timeTo: '09:00 PM'),
      BusinessHours(day: 'Friday', timeFrom: '09:00 AM', timeTo: '09:00 PM'),
      BusinessHours(day: 'Saturday', isClosed: true),
      BusinessHours(day: 'Sunday', isClosed: true),
    ],
  );
}

List<Services> getServiceData() => [
      Services(name: 'Test', image: 'https://www.mye.pw/uploads/vcards/services/41/curve-shape.png', description: loremIpsum),
      Services(name: 'Test Service 2', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', description: loremIpsum),
      Services(name: 'Service 4', image: 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg', description: loremIpsum),
    ];
String loremIpsum =
    '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer''';

class BusinessDatas {
  UserModels userModels;
  List<Services> services;
  List<Galleries> galleries;
  List<Products> products;
  List<Testimonials> testimonials;

  BusinessDatas({
    required this.userModels,
    required this.services,
    required this.galleries,
    required this.products,
    required this.testimonials,
  });
}
