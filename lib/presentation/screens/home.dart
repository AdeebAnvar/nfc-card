import 'package:flutter/material.dart';
// import 'package:flutter_application_1/constants/datas.dart';
import 'package:flutter_application_1/data/models/galleries.dart';
import 'package:flutter_application_1/data/models/products.dart';
import 'package:flutter_application_1/data/models/services.dart';
import 'package:flutter_application_1/data/models/testimonials.dart';
import 'package:flutter_application_1/data/models/user_models.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_1.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_10.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_11.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_12.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_13.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_14.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_15.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_2.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_4.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_5.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_6.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_7.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_8.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_9.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late BusinessDatas _datas;
  late List<Widget> templates = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserModels userModels = getUserData();
    List<Services> services = getServiceData();
    List<Galleries> galleries = getGalleries();
    List<Products> products = getProducts();
    List<Testimonials> testimonials = getTestimonials();

    _datas = BusinessDatas(
      userModels: userModels,
      services: services,
      galleries: galleries,
      products: products,
      testimonials: testimonials,
    );
    templates = [
      Template1(datas: _datas),
      Template2(),
      Template3(),
      Template4(),
      Template5(),
      Template6(),
      Template7(),
      Template8(),
      Template9(datas: _datas),
      Template10(datas: _datas),
      Template11(datas: _datas),
      const Template12(),
      Template13(datas: _datas),
      const Template14(),
      Template15(datas: _datas),
      // Template16(),
      // const Template17(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        padding: const EdgeInsets.all(10),
        itemCount: templates.length,
        itemBuilder: (c, i) => Card(
          color: Colors.grey.shade100,
          surfaceTintColor: Colors.transparent,
          child: ListTile(
            contentPadding: const EdgeInsets.all(10),
            title: Text('Template ${i + 1}'),
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (c) => templates[i])),
            trailing: const Icon(Icons.arrow_forward_ios_outlined),
          ),
        ),
      ),
    );
  }
}
