import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_5.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:intl/intl.dart';

class Template6 extends StatelessWidget {
  const Template6({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 240,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(AppImages.coverImage),
              ),
            ),
            child: Transform.translate(
              offset: const Offset(0, 70),
              child: Container(
                height: 140,
                width: 140,
                margin: const EdgeInsets.only(left: 30),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(AppImages.profilePic),
                  ),
                  boxShadow: [
                    const BoxShadow(
                      color: Colors.black38,
                      blurRadius: 5,
                      spreadRadius: 1,
                    )
                  ],
                  border: Border.all(
                    color: Colors.blue,
                    width: 5,
                  ),
                  borderRadius: BorderRadius.circular(14),
                ),
              ),
            ),
          ),
          const SizedBox(height: 100),
          const Align(
            child: Text(
              'Akhil Sabu',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),
            ),
          ),
          Align(
            child: Text(
              'CEO',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.grey.shade600,
                fontSize: 16,
              ),
            ),
          ),
          Align(
            child: Text(
              'Festa',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.grey.shade600,
                fontSize: 16,
              ),
            ),
          ),
          const SizedBox(height: 20),
          Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Container(
                padding: const EdgeInsets.all(12),
                margin: const EdgeInsets.symmetric(horizontal: 28),
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: AppSocialMediaLinks.socialMediaLinks
                      .map((e) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Icon(e),
                          ))
                      .toList(),
                ),
              ),
              const SizedBox(height: 20),
              const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  '   Just a dummy description',
                  style: TextStyle(fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 20),
              GridView.builder(
                padding: EdgeInsets.all(16),
                shrinkWrap: true,
                itemCount: 6,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 340,
                  mainAxisExtent: 145,
                  crossAxisSpacing: 13,
                  mainAxisSpacing: 16,
                ),
                itemBuilder: (c, i) {
                  return Container(
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(color: Colors.black26, blurRadius: 10, spreadRadius: 1),
                              ],
                              color: Colors.white,
                            ),
                            height: 120,
                            width: 340,
                            padding: EdgeInsets.all(19),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 15),
                                Text(
                                  contactCards[i]['label'],
                                  style: TextStyle(color: Colors.grey.shade700),
                                ),
                                SizedBox(height: 10),
                                Text(
                                  contactCards[i]['content'],
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(color: Colors.grey.shade700),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          left: 10,
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              boxShadow: [
                                // BoxShadow(color: Colors.black26, blurRadius: 3, spreadRadius: 4),
                              ],
                              color: Colors.pink,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.all(8),
                            child: Center(
                              child: Icon(
                                contactCards[i]['icon'],
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(height: 20),
              const Text(
                'QR Code',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
              Container(
                color: Colors.purpleAccent,
                height: 5,
                width: 50,
              ),
              const SizedBox(height: 70),
              Container(
                height: 200,
                margin: const EdgeInsets.symmetric(horizontal: 30),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    const BoxShadow(
                      color: Colors.black,
                      blurRadius: 5,
                      spreadRadius: 1,
                      blurStyle: BlurStyle.outer,
                    ),
                  ],
                ),
                child: Stack(
                  children: [
                    Align(
                      child: Container(
                        margin: const EdgeInsets.only(left: 20, top: 50, bottom: 30),
                        // height: 100,
                        // width: 100,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          // color: Colors.black,
                          // border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Image.network(AppImages.qrCode),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Transform.translate(
                        offset: const Offset(0, -49),
                        child: Container(
                          height: 100,
                          width: 100,
                          margin: const EdgeInsets.only(left: 30),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(AppImages.profilePic),
                              ),
                              boxShadow: [
                                const BoxShadow(
                                  color: Colors.black38,
                                  blurRadius: 5,
                                  spreadRadius: 1,
                                )
                              ],
                              border: Border.all(
                                color: Colors.black,
                                width: 3,
                              ),
                              shape: BoxShape.circle
                              // borderRadius: BorderRadius.circular(30),
                              ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              SizedBox(height: 10),
              Text(
                'Our Services',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
              ),
              Container(
                height: 5,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.orange,
                ),
              ),
              SizedBox(height: 17),
              ListTile(
                contentPadding: EdgeInsets.all(10),
                title: Text(
                  'Test Service 2',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 17,
                  ),
                ),
                subtitle: Text(
                  '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                leading: Image.network('https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
              ),
              SizedBox(height: 20),
              Text(
                'Gallery',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
              ),
              SizedBox(height: 30),
              Container(
                height: 250,
                margin: EdgeInsets.symmetric(horizontal: 23),
                // width: 150,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    )),
              ),
              SizedBox(height: 40),
              Text(
                'Products',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
              ),
              SizedBox(height: 15),
              Card(
                color: Colors.white,
                surfaceTintColor: Colors.white,
                elevation: 4,
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      // width: 200,
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(),
                      width: double.infinity,
                      child: Image.network(fit: BoxFit.cover, 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                    ),
                    Padding(
                      padding: EdgeInsets.all(12),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                'Test Product',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  // fontSize: 14,
                                ),
                              ),
                              Spacer(),
                              Text(
                                '\$2500.00',
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            'Lorem ipsum dolor sit amet income hijack sot y umi',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Text(
                'View More Products',
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const SizedBox(height: 20),
              const Text(
                'Testimonials',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
              Container(
                color: Colors.orangeAccent,
                height: 5,
                width: 50,
              ),
              const SizedBox(height: 20),
              Container(
                height: 400,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.red,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://mye.pw/assets/img/vcard17/testimonial-bg.png',
                    ),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      margin: EdgeInsets.all(18),
                      padding: EdgeInsets.all(18),
                      child: Stack(
                        children: [
                          Column(
                            children: [
                              SizedBox(height: 40),
                              Text(
                                'Test Testimonial',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              ),
                              SizedBox(height: 10),
                              Text(
                                '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                                maxLines: 5,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13,
                                ),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Transform.translate(
                              offset: Offset(0, -84),
                              child: CircleAvatar(
                                radius: 60,
                                backgroundImage: NetworkImage('https://www.mye.pw/uploads/vcards/testimonials/44/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Text(
                'Business Hours',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
              Container(
                color: Colors.amber,
                height: 2,
                width: 20,
              ),
              Padding(
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    SizedBox(height: 30),
                    buildBusinessHours('Monday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Tuesday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Wednesday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Thursday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Friday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Saturday'),
                    SizedBox(height: 10),
                    buildBusinessHours('Sunday'),
                  ],
                ),
              ),
              SizedBox(height: 60),
              Text(
                'Make an Appointment',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
              Container(
                color: Colors.amber,
                height: 2,
                width: 20,
              ),
              SizedBox(height: 20),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '  Date : ',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: datePickerController,
                  onTap: () async {
                    DateTime? dateTime = await showDatePicker(
                      context: context,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2050),
                    );

                    datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      border: OutlineInputBorder(),
                      hintText: 'Pick a Date',
                      fillColor: Colors.white12,
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '  Hour : ',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
              ),
              SizedBox(height: 20),
              TextButton(
                onPressed: () {},
                style: TextButton.styleFrom(
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  shape: RoundedRectangleBorder(),
                ),
                child: Text(
                  'Make an Appointment',
                ),
              ),
              SizedBox(height: 60),
              const SizedBox(height: 20),
              // const Text(
              //   'Inquiries',
              //   style: TextStyle(
              //     fontWeight: FontWeight.w600,
              //     fontSize: 20,
              //   ),
              // ),
              // Container(
              //   color: Colors.purpleAccent,
              //   height: 5,
              //   width: 50,
              // ),
              // const SizedBox(height: 20),
              // Card(
              //   color: Colors.white,
              //   surfaceTintColor: Colors.white,
              //   child: Padding(
              //     padding: const EdgeInsets.all(11),
              //     child: Column(
              //       children: [
              //         CustomTextField(
              //           filledColor: Colors.blue.shade50,
              //           hint: 'Your Name',
              //           label: 'Name',
              //         ),
              //         const SizedBox(height: 20),
              //         CustomTextField(
              //           filledColor: Colors.blue.shade50,
              //           hint: 'Email Address',
              //           label: 'Email',
              //         ),
              //         const SizedBox(height: 20),
              //         CustomTextField(
              //           filledColor: Colors.blue.shade50,
              //           hint: 'Enter Phone Number',
              //           label: 'Phone',
              //         ),
              //         const SizedBox(height: 20),
              //         CustomTextField(
              //           filledColor: Colors.blue.shade50,
              //           hint: 'Type a message here...',
              //           label: 'Message ',
              //           maxLines: 5,
              //         ),
              //         const SizedBox(height: 10),
              //         TextButton(
              //           onPressed: () {},
              //           style: TextButton.styleFrom(
              //               backgroundColor: Colors.black,
              //               foregroundColor: Colors.white,
              //               shape: RoundedRectangleBorder(
              //                 borderRadius: BorderRadius.circular(2),
              //               )),
              //           child: const Text('Send Message'),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
            ],
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }
}
