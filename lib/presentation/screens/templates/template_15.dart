import 'package:flutter/Material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Template15 extends StatelessWidget {
  const Template15({super.key, required this.datas});
  final BusinessDatas datas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 2, 40, 67),
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 2.5,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 3.3,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(datas.userModels.company.coverImage),
                    ),
                  ),
                ),
                Align(alignment: Alignment.bottomCenter, child: ProfilePicCard(datas: datas)),
              ],
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: [
              Text(
                datas.userModels.name,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 19),
              ),
              Text(
                datas.userModels.designation,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 14),
              ),
              Text(
                datas.userModels.company.companyName,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 14),
              ),
              SizedBox(height: 26),
              Text(
                datas.userModels.profileDescription,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 14),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: datas.userModels.company.socialMediaConnections
                .map(
                  (e) => Padding(
                    padding: EdgeInsets.all(10),
                    child: Icon(e.icon, color: Colors.grey, size: 27),
                  ),
                )
                .toList(),
          ),
          Column(
            children: datas.userModels.contactDetails
                .map(
                  (e) => Column(
                    children: [
                      Container(
                        height: 60,
                        width: 60,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(6),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2,
                            color: Colors.blue.shade400,
                          ),
                        ),
                        child: CircleAvatar(
                          backgroundColor: Colors.blue.shade400,
                          child: Center(
                            child: Icon(
                              e.icon,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        e.label,
                        style: TextStyle(fontWeight: FontWeight.w400, color: Colors.white),
                      ),
                      SizedBox(height: 5),
                      Text(
                        e.content,
                        style: TextStyle(fontWeight: FontWeight.w400, color: Colors.white),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'QR Code',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.all(17),
            margin: EdgeInsets.all(17),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                ProfilePicCard(datas: datas),
                SizedBox(height: 20),
                Container(
                  height: 140,
                  width: 140,
                  decoration: BoxDecoration(
                    // color: Colors.white38,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        blurStyle: BlurStyle.inner,
                        color: Colors.black12,
                        spreadRadius: 4,
                        blurRadius: 7,
                      )
                    ],
                  ),
                  padding: EdgeInsets.all(14),
                  child: Image.network(datas.userModels.company.qrCode),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Our Services',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: datas.services
                .map(
                  (e) => Container(
                    padding: EdgeInsets.all(17),
                    margin: EdgeInsets.all(17),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all()),
                    child: Column(
                      children: [
                        Image.network(height: 150, width: 150, fit: BoxFit.cover, e.image),
                        SizedBox(height: 20),
                        Text(
                          e.name,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          e.description,
                          style: TextStyle(
                            color: Colors.white60,
                            fontWeight: FontWeight.w400,
                            fontSize: 13,
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Gallery',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          FlutterCarousel.builder(
            itemCount: datas.galleries.length,
            options: CarouselOptions(
              height: 300.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: CircularSlideIndicator(
                indicatorBackgroundColor: Colors.black,
                currentIndicatorColor: Colors.green.shade900,
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 10, color: Colors.blue),
                    // borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        datas.galleries[i].image,
                      ),
                    ),
                  ),
                  height: 250,
                  margin: const EdgeInsets.all(20),
                ),
              );
            },
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Products',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          FlutterCarousel.builder(
            itemCount: datas.products.length,
            options: CarouselOptions(
              height: 400.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: CircularSlideIndicator(
                indicatorBackgroundColor: Colors.black,
                currentIndicatorColor: Colors.green.shade900,
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Container(
                padding: EdgeInsets.all(15),
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.network(
                      height: 200,
                      width: MediaQuery.sizeOf(context).width,
                      datas.products[i].image,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(height: 20),
                    Text(
                      datas.products[i].name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ),
                    ),
                    Text(
                      datas.products[i].description,
                      maxLines: 4,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          SizedBox(height: 10),
          Align(
            child: Text(
              'View More Products',
              style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline, decorationColor: Colors.blue),
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Testimonials',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.all(17),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(
                    datas.testimonials[0].image,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  datas.testimonials[0].label.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  datas.testimonials[0].testimonial.toString(),
                  maxLines: 3,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),

          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Business Hours',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.company.businessHours
                .map(
                  (e) => Container(
                    margin: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                    height: 90,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.blue,
                      ),
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: ListTile(
                        subtitle: Text(
                          e.isClosed ? 'Closed' : '${e.timeFrom} - ${e.timeTo}',
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                        title: Text(
                          e.day,
                          style: TextStyle(
                            color: Color.fromARGB(255, 52, 98, 226),
                          ),
                        ),
                        leading: Container(
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color.fromARGB(255, 28, 52, 119),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.calendar_month,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Container(
            height: 1,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            margin: EdgeInsets.symmetric(horizontal: 50),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Make an Appointment',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 20),
          Text(
            '  Date',
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: DatePickerField(
              fillColor: Colors.blue,
              hintTextColor: Colors.white,
              suffixIconColor: Colors.transparent,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Text(
            '  Hour',
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(color: Colors.black26, spreadRadius: 2, blurRadius: 4),
              ],
            ),
            child: Center(
              child: Text(
                'Make an Appointment',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          SizedBox(height: 50),
          // Container(
          //   height: 250,
          //   child: Stack(
          //     children: [
          //       Image.network(
          //         height: 200,
          //         width: MediaQuery.sizeOf(context).width,
          //         fit: BoxFit.cover,
          //         AppImages.coverImage,
          //       ),
          //       Align(
          //         alignment: Alignment.bottomCenter,
          //         child: Container(
          //           height: 100,
          //           width: 100,
          //           decoration: BoxDecoration(
          //             shape: BoxShape.circle,
          //             image: DecorationImage(
          //               image: NetworkImage(AppImages.profilePic),
          //             ),
          //             border: Border.all(
          //               color: Colors.white,
          //               width: 3,
          //             ),
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // ),
          // SizedBox(height: 22),
          // Padding(
          //   padding: EdgeInsets.all(18.0),
          //   child: Column(
          //     children: [
          //       Text(
          //         'CEO',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontSize: 16,
          //           fontWeight: FontWeight.w500,
          //         ),
          //       ),
          //       SizedBox(height: 20),
          //       Text(
          //         'Just a dummy description',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontSize: 12,
          //           fontWeight: FontWeight.w400,
          //         ),
          //       ),
          //       SizedBox(height: 30),
          //       Divider(
          //         color: Colors.white,
          //         indent: 100,
          //         endIndent: 100,
          //       ),
          //       SizedBox(height: 15),
          //       Text(
          //         'QR Code',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontSize: 18,
          //           fontWeight: FontWeight.w400,
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       Container(
          //         height: 150,
          //         margin: EdgeInsets.all(19),
          //         decoration: BoxDecoration(
          //           color: Colors.white,
          //           borderRadius: BorderRadius.circular(12),
          //         ),
          //         child: Row(
          //           mainAxisAlignment: MainAxisAlignment.center,
          //           children: [
          //             Container(
          //               width: 100,
          //               height: 100,
          //               padding: EdgeInsets.all(8),
          //               decoration: BoxDecoration(
          //                 borderRadius: BorderRadius.circular(10),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.shade200,
          //                     // blurRadius: 1,
          //                     spreadRadius: 2,
          //                     blurStyle: BlurStyle.inner,
          //                   )
          //                 ],
          //               ),
          //               child: Image.network(AppImages.qrCode),
          //             ),
          //             SizedBox(width: 10),
          //             Container(
          //               height: 100,
          //               width: 100,
          //               decoration: BoxDecoration(
          //                 shape: BoxShape.circle,
          //                 image: DecorationImage(
          //                   image: NetworkImage(AppImages.profilePic),
          //                 ),
          //                 border: Border.all(
          //                   color: Colors.white,
          //                   width: 3,
          //                 ),
          //               ),
          //             ),
          //           ],
          //         ),
          //       ),
          //       SizedBox(height: 30),
          //       Divider(
          //         color: Colors.white,
          //         indent: 100,
          //         endIndent: 100,
          //       ),
          //       SizedBox(height: 15),
          //       Text(
          //         'Inquiries',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontSize: 18,
          //           fontWeight: FontWeight.w400,
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       TextField(
          //         decoration: InputDecoration(
          //           fillColor: Color.fromARGB(255, 43, 72, 95),
          //           filled: true,
          //           hintText: 'Your Name',
          //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          //           hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
          //           enabledBorder: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //           border: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       TextField(
          //         decoration: InputDecoration(
          //           fillColor: Color.fromARGB(255, 43, 72, 95),
          //           filled: true,
          //           hintText: 'Enter Email Address',
          //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          //           hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
          //           enabledBorder: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //           border: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       TextField(
          //         decoration: InputDecoration(
          //           fillColor: Color.fromARGB(255, 43, 72, 95),
          //           filled: true,
          //           hintText: 'Enter Phone',
          //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          //           hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
          //           enabledBorder: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //           border: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       TextField(
          //         decoration: InputDecoration(
          //           fillColor: Color.fromARGB(255, 43, 72, 95),
          //           filled: true,
          //           hintText: 'Type a message here...',
          //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          //           hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
          //           enabledBorder: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //           border: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white24),
          //           ),
          //         ),
          //       ),
          //       SizedBox(height: 15),
          //       TextButton(
          //         onPressed: () {},
          //         child: Text('Send Message'),
          //         style: TextButton.styleFrom(
          //           backgroundColor: Colors.blue,
          //           foregroundColor: Colors.white,
          //           shape: RoundedRectangleBorder(
          //             borderRadius: BorderRadius.circular(10),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}
