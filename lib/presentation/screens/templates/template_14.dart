import 'dart:ui';

import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:intl/intl.dart';

class Template14 extends StatelessWidget {
  const Template14({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.black,
      body: ListView(
        children: [
          Container(
            height: 380,
            child: Stack(
              children: [
                Stack(
                  children: [
                    Image.network(
                      AppImages.coverImage,
                      height: 300,
                      fit: BoxFit.cover,
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 100,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.transparent,
                              Colors.black,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(height: 30),
                      CircleAvatar(
                        radius: 60,
                        backgroundImage: NetworkImage(AppImages.profilePic),
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Akhil Sabu',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        'CEO',
                        style: TextStyle(
                          color: Colors.orange.shade700,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        'Festa',
                        style: TextStyle(
                          color: Colors.orange.shade700,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              SizedBox(height: 40),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '      Just a dummy description',
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 15, color: Colors.white),
                ),
              ),
              SizedBox(height: 40),
              Divider(
                height: 2,
                color: Colors.grey[800],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: AppSocialMediaLinks.socialMediaLinks
                    .map((e) => Icon(
                          e,
                          color: Colors.amber,
                        ))
                    .toList(),
              ),
              SizedBox(height: 20),
              Divider(
                height: 2,
                color: Colors.grey[800],
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  Container(
                    width: 50,
                    color: Colors.orange.shade700,
                    height: 40,
                  ),
                  SizedBox(width: 10),
                  Text(
                    'Contact',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                    ),
                  )
                ],
              ),
              SizedBox(height: 20),
              Column(
                children: contactCards.map(
                  (e) {
                    return ListTile(
                      leading: Card(
                        shape: RoundedRectangleBorder(),
                        surfaceTintColor: Colors.white10,
                        color: Colors.white10,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            e['icon'],
                            color: Colors.white,
                          ),
                        ),
                      ),
                      title: Text(
                        e['label'],
                        style: TextStyle(
                          color: Colors.orange.shade700,
                        ),
                      ),
                      subtitle: Text(
                        e['content'],
                        style: TextStyle(color: Colors.white),
                      ),
                    );
                  },
                ).toList(),
              ),
              SizedBox(height: 60),
              Text(
                'QR Code',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 20),
              Container(
                height: 250,
                width: MediaQuery.sizeOf(context).width / 1.4,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 250 / 1.2,
                        color: Colors.white,
                        padding: EdgeInsets.only(top: 40),
                        child: Align(
                          child: Image.network(
                            height: 100,
                            width: 100,
                            AppImages.qrCode,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.black, width: 5),
                          image: DecorationImage(
                            image: NetworkImage(AppImages.profilePic),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Text(
                'Our Services',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                leading: Container(
                  // height: 100,
                  width: 70,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    ),
                  ),
                ),
                title: Text(
                  'Test ',
                  style: TextStyle(
                    color: Colors.orange.shade700,
                  ),
                ),
                subtitle: Text(
                  '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                  maxLines: 5,
                  style: TextStyle(color: Colors.white54),
                ),
              ),
              ListTile(
                leading: Container(
                  // height: 100,
                  width: 70,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    ),
                  ),
                ),
                title: Text(
                  'Test ',
                  style: TextStyle(
                    color: Colors.orange.shade700,
                  ),
                ),
                subtitle: Text(
                  '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                  maxLines: 5,
                  style: TextStyle(color: Colors.white54),
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Gallery',
              style: TextStyle(
                fontSize: 17,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 200,
            margin: EdgeInsets.symmetric(horizontal: 38),
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                ),
              ),
            ),
          ),
          SizedBox(height: 30),
          Row(
            children: [
              Container(
                width: 50,
                color: Colors.orange.shade700,
                height: 40,
              ),
              SizedBox(width: 10),
              Text(
                'Products',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Align(
            child: Container(
              height: 300,
              width: 240,
              // color: Colors.red,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 155,
                      decoration: BoxDecoration(
                        color: Colors.white12,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  'Test Product',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  '2500.00',
                                  style: TextStyle(
                                    color: Colors.yellow.shade800,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                              maxLines: 3,
                              style: TextStyle(
                                color: Colors.white30,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: 200,
                      height: 165,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'View More Products',
              style: TextStyle(
                color: Colors.white,
                decoration: TextDecoration.underline,
                decorationColor: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 40),
          Align(
            child: Text(
              'Testimonials',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Container(
              height: 200,
              padding: EdgeInsets.all(15),
              margin: EdgeInsets.symmetric(
                horizontal: 30,
              ),
              color: Colors.white12,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    maxLines: 3,
                    style: TextStyle(
                      color: Colors.white38,
                    ),
                  ),
                  SizedBox(height: 10),
                  ListTile(
                    leading: CircleAvatar(radius: 30),
                    title: Text(
                      'Test Testimonial',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 40),
          Row(
            children: [
              Container(
                width: 50,
                color: Colors.orange.shade700,
                height: 40,
              ),
              SizedBox(width: 10),
              Text(
                'Make an Appointment',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Text(
            '  Date',
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: TextField(
              controller: datePickerController,
              onTap: () async {
                DateTime? dateTime = await showDatePicker(
                  context: context,
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2050),
                );

                datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
              },
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  border: OutlineInputBorder(),
                  hintText: 'Pick a Date',
                  hintStyle: TextStyle(color: Colors.white54),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white38),
                    borderRadius: BorderRadius.circular(3),
                  )),
            ),
          ),
          Text(
            '  Hour',
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: TextButton(
              onPressed: () {},
              child: Text('Make an Appointment'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.yellow.shade800,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(),
                fixedSize: Size(MediaQuery.sizeOf(context).width, 40),
              ),
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                width: 50,
                color: Colors.orange.shade700,
                height: 40,
              ),
              SizedBox(width: 10),
              Text(
                'Business Hours',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Container(
            color: Colors.white10,
            margin: EdgeInsets.all(24),
            padding: EdgeInsets.all(18),
            child: Column(
              children: [
                buildRow('Monday'),
                SizedBox(height: 10),
                buildRow('Tuesday'),
                SizedBox(height: 10),
                buildRow('Wednesday'),
                SizedBox(height: 10),
                buildRow('Thursday'),
                SizedBox(height: 10),
                buildRow('Friday'),
                SizedBox(height: 10),
                buildRow('Saturday'),
                SizedBox(height: 10),
                buildRow('Sunday'),
              ],
            ),
          ),
          // Text(
          //   'Inquiries',
          //   style: TextStyle(
          //     color: Colors.white,
          //     fontSize: 22,
          //     fontWeight: FontWeight.w600,
          //   ),
          // ),
          // SizedBox(height: 20),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 0.0),
          //   child: Wrap(
          //     spacing: 13,
          //     runSpacing: 3,
          //     children: [
          //       ConstrainedBox(
          //         constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.4),
          //         child: TextField(
          //           decoration: InputDecoration(
          //             hintText: 'Your Name',
          //             hintStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.w400),
          //             enabledBorder: OutlineInputBorder(
          //               borderSide: BorderSide(color: Colors.white24),
          //             ),
          //             border: OutlineInputBorder(
          //               borderSide: BorderSide(color: Colors.white24),
          //             ),
          //           ),
          //         ),
          //       ),
          //       ConstrainedBox(
          //         constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.4),
          //         child: TextField(
          //           decoration: InputDecoration(
          //             hintText: 'Enter Phone Number',
          //             hintStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.w400),
          //             enabledBorder: OutlineInputBorder(
          //               borderSide: BorderSide(color: Colors.white24),
          //             ),
          //             border: OutlineInputBorder(
          //               borderSide: BorderSide(color: Colors.white24),
          //             ),
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // ),
          // SizedBox(height: 10),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 22.0),
          //   child: TextField(
          //     decoration: InputDecoration(
          //       hintText: 'Email Address',
          //       hintStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.w400),
          //       enabledBorder: OutlineInputBorder(
          //         borderSide: BorderSide(color: Colors.white24),
          //       ),
          //       border: OutlineInputBorder(
          //         borderSide: BorderSide(color: Colors.white24),
          //       ),
          //     ),
          //   ),
          // ),
          // SizedBox(height: 10),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 22.0),
          //   child: TextField(
          //     maxLines: 4,
          //     decoration: InputDecoration(
          //       hintText: 'Type a message here . . ',
          //       hintStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.w400),
          //       enabledBorder: OutlineInputBorder(
          //         borderSide: BorderSide(color: Colors.white24),
          //       ),
          //       border: OutlineInputBorder(
          //         borderSide: BorderSide(color: Colors.white24),
          //       ),
          //     ),
          //   ),
          // ),
          // SizedBox(height: 15),
          // TextButton(
          //   style: TextButton.styleFrom(
          //     backgroundColor: Colors.orange.shade700,
          //     foregroundColor: Colors.white,
          //     shape: RoundedRectangleBorder(),
          //   ),
          //   onPressed: () {},
          //   child: Text('Send Message'),
          // ),
          SizedBox(height: 60),
        ],
      ),
    );
  }

  Row buildRow(String label) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: TextStyle(color: Colors.white),
        ),
        Text(
          '09:00 AM - 09:00 PM',
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }
}
