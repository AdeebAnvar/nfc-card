import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Template13 extends StatelessWidget {
  const Template13({super.key, required this.datas});
  final BusinessDatas datas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: ListView(
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 2.6,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 4,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        datas.userModels.company.coverImage,
                      ),
                    ),
                  ),
                ),
                Align(alignment: Alignment.bottomCenter, child: ProfilePicCard(datas: datas, isRounded: true, borderColor: Colors.white)),
              ],
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: [
              Text(
                datas.userModels.name,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
              ),
              SizedBox(height: 7),
              Text(datas.userModels.designation),
              SizedBox(height: 7),
              Text(datas.userModels.company.companyName),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: datas.userModels.company.socialMediaConnections
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 40,
                      width: 40,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(),
                      ),
                      child: Center(
                        child: Icon(e.icon),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 10),
          Text('    ${datas.userModels.profileDescription}'),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.contactDetails
                .map(
                  (e) => Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.sizeOf(context).width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.green.shade100,
                    ),
                    child: Column(
                      children: [
                        Icon(e.icon, color: Colors.teal.shade800, size: 47),
                        SizedBox(height: 10),
                        Text(e.content),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Container(
            color: Colors.green.shade100,
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Text(
                  'QR Code',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    color: const Color.fromARGB(255, 12, 52, 14),
                  ),
                ),
                SizedBox(height: 50),
                Container(
                  height: 200,
                  margin: EdgeInsets.symmetric(horizontal: 30),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 5,
                        spreadRadius: 1,
                        blurStyle: BlurStyle.outer,
                      ),
                    ],
                  ),
                  child: Stack(
                    children: [
                      Align(
                        child: Container(
                          margin: EdgeInsets.only(left: 20, top: 50, bottom: 30, right: 20),
                          // height: 100,
                          // width: 100,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            // color: Colors.black,
                            // border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Image.network(AppImages.qrCode),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Transform.translate(
                          offset: Offset(0, -49),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(AppImages.profilePic),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.white,
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                  )
                                ],
                                border: Border.all(
                                  color: Colors.white,
                                  width: 3,
                                ),
                                shape: BoxShape.circle
                                // borderRadius: BorderRadius.circular(30),
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Our Services',
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.teal.shade800),
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              children: datas.services
                  .map(
                    (e) => Card(
                      color: Colors.white,
                      surfaceTintColor: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 18),
                              height: 160,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: NetworkImage(e.image),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Text(e.name),
                            Text(
                              e.description,
                              maxLines: 4,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Gallery',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),

          FlutterCarousel.builder(
            itemCount: datas.galleries.length,
            options: CarouselOptions(
              height: 400.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: const CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Card(
                  color: Colors.white,
                  surfaceTintColor: Colors.white,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      // borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          datas.galleries[i].image,
                        ),
                      ),
                    ),
                    height: 250,
                    margin: const EdgeInsets.all(20),
                  ),
                ),
              );
            },
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Products',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),

          FlutterCarousel.builder(
            itemCount: datas.products.length,
            options: CarouselOptions(
              height: 400.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: const CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 4,
                      blurRadius: 5,
                    ),
                  ],
                ),
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      height: 170,
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.red,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(datas.products[i].image),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            datas.products[i].name,
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(
                            datas.products[i].description,
                            maxLines: 3,
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(datas.products[i].price)
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
          SizedBox(height: 10),
          Align(
            child: Text(
              'View More Products',
              style: TextStyle(
                color: Colors.green,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          SizedBox(height: 30),
          Align(
            child: Text(
              'Testimonials',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            margin: EdgeInsets.all(18),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black38,
                  spreadRadius: 4,
                  blurRadius: 8,
                )
              ],
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(datas.testimonials[0].image),
                ),
                SizedBox(height: 20),
                Text(
                  datas.testimonials[0].testimonial,
                  maxLines: 5,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                ),
                SizedBox(height: 20),
                Container(
                  height: 6,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.green.shade600,
                  ),
                ),
                SizedBox(height: 10),
                Text(datas.testimonials[0].label.toString()),
                SizedBox(height: 40),
              ],
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Business Hours',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.company.businessHours
                .map(
                  (e) => Container(
                    margin: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                    height: 90,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 2,
                          spreadRadius: 1,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: ListTile(
                        subtitle: Text(
                          e.isClosed ? 'Closed' : '${e.timeFrom} - ${e.timeTo}',
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        title: Text(e.day),
                        leading: Container(
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.green.shade800,
                          ),
                          child: Center(
                            child: Icon(
                              Icons.calendar_month,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Make an Appointment',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),
          SizedBox(height: 20),
          Container(
            // height: 100,
            margin: EdgeInsets.all(15),
            padding: EdgeInsets.all(15),
            width: MediaQuery.sizeOf(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 4,
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Date'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DatePickerField(
                    suffixIconColor: Colors.transparent,
                    fillColor: Colors.green.shade50,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                Text('Hour'),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.green.shade800,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(color: Colors.black26, spreadRadius: 2, blurRadius: 4),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Make an Appointment',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 80),
          // Container(
          //   height: MediaQuery.sizeOf(context).height / 2.3,
          //   child: Stack(
          //     children: [
          //       Container(
          //         height: MediaQuery.sizeOf(context).height / 3.8,
          //         decoration: BoxDecoration(
          //           image: DecorationImage(
          //             fit: BoxFit.cover,
          //             image: NetworkImage(AppImages.coverImage),
          //           ),
          //         ),
          //       ),
          //       Align(
          //         alignment: Alignment.bottomLeft,
          //         child: Row(
          //           children: [
          //             Container(
          //               height: 150,
          //               width: 150,
          //               decoration: BoxDecoration(
          //                 border: Border.all(color: Colors.white, width: 4),
          //                 shape: BoxShape.circle,
          //                 image: DecorationImage(
          //                   image: NetworkImage(AppImages.profilePic),
          //                 ),
          //               ),
          //             ),
          //             SizedBox(width: 20),
          //             Text(
          //               'CEO',
          //               style: TextStyle(
          //                 fontWeight: FontWeight.w400,
          //                 fontSize: 16,
          //                 color: Colors.green,
          //               ),
          //             ),
          //           ],
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // SizedBox(height: 30),
          // Text(
          //   '    Just a dummy description',
          //   style: TextStyle(
          //     fontWeight: FontWeight.w400,
          //     fontSize: 13,
          //     color: Colors.grey,
          //   ),
          // ),
          // SizedBox(height: 30),
          // Container(
          //   color: Colors.green.shade100,
          //   padding: EdgeInsets.all(10),
          //   child: Column(
          //     children: [
          //       Text(
          //         'QR Code',
          //         style: TextStyle(
          //           fontWeight: FontWeight.w600,
          //           fontSize: 20,
          //           color: const Color.fromARGB(255, 12, 52, 14),
          //         ),
          //       ),
          //       SizedBox(height: 50),
          //       Container(
          //         height: 200,
          //         margin: EdgeInsets.symmetric(horizontal: 30),
          //         decoration: BoxDecoration(
          //           color: Colors.white,
          //           borderRadius: BorderRadius.circular(10),
          //           boxShadow: [
          //             BoxShadow(
          //               color: Colors.black26,
          //               blurRadius: 5,
          //               spreadRadius: 1,
          //               blurStyle: BlurStyle.outer,
          //             ),
          //           ],
          //         ),
          //         child: Stack(
          //           children: [
          //             Align(
          //               child: Container(
          //                 margin: EdgeInsets.only(left: 20, top: 50, bottom: 30, right: 20),
          //                 // height: 100,
          //                 // width: 100,
          //                 padding: EdgeInsets.all(10),
          //                 decoration: BoxDecoration(
          //                   // color: Colors.black,
          //                   // border: Border.all(color: Colors.black),
          //                   borderRadius: BorderRadius.circular(16),
          //                 ),
          //                 child: Image.network(AppImages.qrCode),
          //               ),
          //             ),
          //             Align(
          //               alignment: Alignment.topCenter,
          //               child: Transform.translate(
          //                 offset: Offset(0, -49),
          //                 child: Container(
          //                   height: 100,
          //                   width: 100,
          //                   decoration: BoxDecoration(
          //                       image: DecorationImage(
          //                         image: NetworkImage(AppImages.profilePic),
          //                       ),
          //                       boxShadow: [
          //                         BoxShadow(
          //                           color: Colors.white,
          //                           blurRadius: 5,
          //                           spreadRadius: 1,
          //                         )
          //                       ],
          //                       border: Border.all(
          //                         color: Colors.white,
          //                         width: 3,
          //                       ),
          //                       shape: BoxShape.circle
          //                       // borderRadius: BorderRadius.circular(30),
          //                       ),
          //                 ),
          //               ),
          //             ),
          //           ],
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // SizedBox(height: 20),
          // Align(
          //   child: Text(
          //     'Inquiries',
          //     style: TextStyle(
          //       fontWeight: FontWeight.w600,
          //       fontSize: 20,
          //       color: Colors.green.shade900,
          //     ),
          //   ),
          // ),
          // SizedBox(height: 10),
          // Padding(
          //   padding: const EdgeInsets.all(13.0),
          //   child: Column(
          //     children: [
          //       CustomTextField(
          //         hint: 'Your Name',
          //         label: 'Name',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hint: 'Email Address',
          //         label: 'Email',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hint: 'Enter Phone Number',
          //         label: 'Phone',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hint: 'Type a message here...',
          //         label: 'Message ',
          //         maxLines: 5,
          //       ),
          //     ],
          //   ),
          // ),
          // SizedBox(height: 10),
          // Align(
          //   child: TextButton(
          //     onPressed: () {},
          //     style: TextButton.styleFrom(
          //         backgroundColor: Colors.green.shade800,
          //         foregroundColor: Colors.white,
          //         shape: RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(10),
          //         )),
          //     child: Text('Send Message'),
          //   ),
          // )
        ],
      ),
    );
  }
}
