import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:flutter_application_1/widgets/custom_container.dart';
import 'package:intl/intl.dart';

class Template8 extends StatelessWidget {
  const Template8({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();

    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 47, 39),
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipPath(
                clipper: CustomContainertemplate8(),
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(AppImages.coverImage),
                    ),
                  ),
                  padding: const EdgeInsets.all(18),
                  height: 250,
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              Transform.translate(
                offset: const Offset(0, -60),
                child: Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.yellow, width: 3),
                        image: DecorationImage(
                          image: NetworkImage(AppImages.profilePic),
                        ),
                      ),
                    )),
              ),
            ],
          ),
          Transform.translate(
            offset: const Offset(0, -40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Akhil Sabu',
                        style: TextStyle(
                          fontSize: 24,
                          color: Color(0xFFFFD700),
                        ),
                      ),
                      Text(
                        'CEO',
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        'Festa',
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Just a dummy description\n',
                        style: TextStyle(fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: AppSocialMediaLinks.socialMediaLinks
                      .map(
                        (e) => Card(
                          elevation: 5,
                          color: const Color.fromARGB(255, 7, 69, 58),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Icon(
                                e,
                                color: const Color(0xFFFFD700),
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                const SizedBox(height: 40),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 4,
                        color: Colors.yellow,
                      ),
                    ),
                    const SizedBox(width: 40),
                    const Text(
                      'Contact',
                      style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22, color: Colors.white),
                    ),
                    const SizedBox(width: 40),
                    Expanded(
                      child: Container(
                        height: 4,
                        color: Colors.yellow,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Column(
                  children: contactCards.map(
                    (e) {
                      return ListTile(
                        leading: Card(
                          surfaceTintColor: const Color.fromARGB(255, 7, 69, 58),
                          color: const Color.fromARGB(255, 7, 69, 58),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              e['icon'],
                              color: const Color.fromARGB(255, 161, 145, 54),
                            ),
                          ),
                        ),
                        title: Text(
                          e['label'],
                          style: const TextStyle(
                            color: Colors.white54,
                          ),
                        ),
                        subtitle: Text(
                          e['content'],
                          style: const TextStyle(color: Colors.white),
                        ),
                      );
                    },
                  ).toList(),
                ),
                const SizedBox(height: 20),
                Container(
                  color: Colors.grey.shade100,
                  width: double.infinity,
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      const Text(
                        'Our Services',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 17,
                        ),
                      ),
                      const SizedBox(height: 40),
                      Container(
                        height: 300,
                        width: MediaQuery.sizeOf(context).width,
                        margin: const EdgeInsets.all(18),
                        padding: const EdgeInsets.all(18),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.orange.shade600),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: [
                            Image.network(
                              height: 100,
                              width: 100,
                              'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                            ),
                            const Text(
                              'Test',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(height: 15),
                            const Text(
                              '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                              maxLines: 4,
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 4,
                        color: Colors.yellow,
                      ),
                    ),
                    const SizedBox(width: 40),
                    const Text(
                      'QR Code',
                      style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22, color: Colors.white),
                    ),
                    const SizedBox(width: 40),
                    Expanded(
                      child: Container(
                        height: 4,
                        color: Colors.yellow,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 30),
                Align(
                  child: Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                          width: 20,
                          color: Colors.teal.shade900,
                        ),
                        boxShadow: [
                          const BoxShadow(
                            color: Colors.black26,
                            blurRadius: 12,
                          )
                        ]),
                    child: Image.network(AppImages.qrCode),
                  ),
                ),
                const SizedBox(height: 50),
                Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      const SizedBox(height: 20),
                      const Text(
                        'Gallery',
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        height: 300,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 40),
                      Container(
                        width: MediaQuery.sizeOf(context).width,
                        padding: const EdgeInsets.all(18),
                        color: Colors.grey.shade200,
                        child: Column(
                          children: [
                            const Text(
                              'Products',
                              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(height: 20),
                            Container(
                              height: 230,
                              margin: const EdgeInsets.symmetric(horizontal: 70),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  const BoxShadow(
                                    color: Colors.black26,
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                  )
                                ],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    height: 110,
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                      ),
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                          'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 10),
                                  Padding(
                                    padding: const EdgeInsets.all(3),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            const Text(
                                              'Test Product',
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                            const Spacer(),
                                            Text(
                                              '\$2880.00',
                                              style: TextStyle(color: Colors.yellow.shade800, fontWeight: FontWeight.w700),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(height: 14),
                                        const Text(
                                          '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                                          maxLines: 3,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 16),
                            const Text(
                              'View More Products',
                              style: TextStyle(decoration: TextDecoration.underline),
                            ),
                            const SizedBox(height: 16),
                          ],
                        ),
                      ),
                      // Text(
                      //   'Inquiries',
                      //   style: TextStyle(
                      //     fontWeight: FontWeight.w600,
                      //     fontSize: 20,
                      //   ),
                      // ),
                      // SizedBox(height: 15),
                      // Wrap(
                      //   runSpacing: 20,
                      //   spacing: 20,
                      //   children: [
                      //     ConstrainedBox(
                      //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.25),
                      //       child: TextField(
                      //         style: TextStyle(color: Colors.grey),
                      //         decoration: InputDecoration(
                      //           focusColor: Colors.grey,
                      //           hintText: 'Your Name',
                      //           hintStyle: TextStyle(color: Colors.grey),
                      //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      //           enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //           focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //           border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //         ),
                      //       ),
                      //     ),
                      //     ConstrainedBox(
                      //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.25),
                      //       child: TextField(
                      //         style: TextStyle(color: Colors.grey),
                      //         decoration: InputDecoration(
                      //           focusColor: Colors.grey,
                      //           hintText: 'Email Address',
                      //           hintStyle: TextStyle(color: Colors.grey),
                      //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      //           enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //           focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //           border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //         ),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(height: 15),
                      // TextField(
                      //   style: TextStyle(color: Colors.grey),
                      //   decoration: InputDecoration(
                      //     focusColor: Colors.grey,
                      //     hintText: 'Enter Phone Number',
                      //     hintStyle: TextStyle(color: Colors.grey),
                      //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      //     enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //     focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //     border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //   ),
                      // ),
                      // SizedBox(height: 15),
                      // TextField(
                      //   style: TextStyle(color: Colors.grey),
                      //   decoration: InputDecoration(
                      //     focusColor: Colors.grey,
                      //     hintText: 'Type a message here',
                      //     hintStyle: TextStyle(color: Colors.grey),
                      //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      //     enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //     focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //     border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      //   ),
                      // ),
                      // SizedBox(height: 24),
                      // TextButton(
                      //   onPressed: () {},
                      //   style: TextButton.styleFrom(
                      //     backgroundColor: Colors.brown,
                      //     foregroundColor: Colors.white,
                      //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                      //   ),
                      //   child: Text(
                      //     'Send Message',
                      //   ),
                      // )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 4,
                  color: Colors.yellow,
                ),
              ),
              const SizedBox(width: 40),
              const Text(
                'Testimonials',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22, color: Colors.white),
              ),
              const SizedBox(width: 40),
              Expanded(
                child: Container(
                  height: 4,
                  color: Colors.yellow,
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Container(
            height: 140,
            margin: const EdgeInsets.symmetric(horizontal: 120, vertical: 30),
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black45,
                  spreadRadius: 10,
                  blurRadius: 10,
                )
              ],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(90),
                bottomLeft: Radius.circular(90),
                bottomRight: Radius.circular(90),
              ),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  'https://www.mye.pw/uploads/vcards/services/47/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 18.0),
            child: Text(
              '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
              maxLines: 5,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          const SizedBox(height: 20),
          Align(
            child: Text(
              'Test Testimonial',
              style: TextStyle(
                color: Colors.yellow.shade900,
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            color: Colors.white,
            width: MediaQuery.sizeOf(context).width,
            child: Column(
              children: [
                const SizedBox(height: 20),
                const Text(
                  'Business Hours',
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
                ),
                const SizedBox(height: 20),
                Container(
                  width: MediaQuery.sizeOf(context).width / 1.4,
                  padding: const EdgeInsets.all(19),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      const BoxShadow(
                        color: Colors.black38,
                        spreadRadius: 10,
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: const Column(
                    children: [
                      Text('Monday: 09:00 AM - 09:00 PM'),
                      SizedBox(height: 13),
                      Text('Tuesday: 09:00 AM - 09:00 PM'),
                      SizedBox(height: 13),
                      Text('Wednesday: 09:00 AM - 09:00 PM'),
                      SizedBox(height: 13),
                      Text('Thursday: 09:00 AM - 09:00 PM'),
                      SizedBox(height: 13),
                      Text('Friday: 09:00 AM - 09:00 PM'),
                      SizedBox(height: 13),
                      Text('Saturday: Closed'),
                      SizedBox(height: 13),
                      Text('Sunday: Closed'),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
          const SizedBox(height: 30),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 4,
                  color: Colors.yellow,
                ),
              ),
              const SizedBox(width: 40),
              const Text(
                'Make an Appointment',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22, color: Colors.white),
              ),
              const SizedBox(width: 40),
              Expanded(
                child: Container(
                  height: 4,
                  color: Colors.yellow,
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(11.0),
            child: TextField(
              controller: datePickerController,
              onTap: () async {
                DateTime? dateTime = await showDatePicker(context: context, firstDate: DateTime(2000), lastDate: DateTime(2050));

                datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
              },
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  border: const OutlineInputBorder(),
                  hintText: 'Pick a Date',
                  hintStyle: const TextStyle(color: Colors.white54),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white38),
                    borderRadius: BorderRadius.circular(10),
                  )),
            ),
          ),
          const SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 48.0),
            child: TextButton(
              onPressed: () {},
              child: const Text('Make an Appointment'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.yellow.shade800,
                foregroundColor: Colors.white,
                shape: const RoundedRectangleBorder(),
                fixedSize: Size(MediaQuery.sizeOf(context).width, 40),
              ),
            ),
          ),
          const SizedBox(height: 100),
        ],
      ),
    );
  }
}
