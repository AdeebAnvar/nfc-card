import 'package:flutter/Material.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';

class Template12 extends StatelessWidget {
  const Template12({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade50,
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: ListView(
        children: [
          Container(
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(AppImages.coverImage),
              ),
            ),
          ),
          SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 130,
                width: 130,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 4),
                  image: DecorationImage(
                    image: NetworkImage(AppImages.profilePic),
                  ),
                ),
              ),
              SizedBox(width: 13),
              Text(
                'CEO',
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: Colors.blue,
                ),
              ),
            ],
          ),
          SizedBox(height: 30),
          Text(
            '    Just a dummy description',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
          ),
          SizedBox(height: 30),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Text(
                  'QR Code',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    color: Colors.blue.shade800,
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.network(height: 100, AppImages.qrCode),
                    Container(
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 4),
                        image: DecorationImage(
                          image: NetworkImage(AppImages.profilePic),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Inquiries',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: Colors.blue.shade800,
              ),
            ),
          ),
          SizedBox(height: 10),
          Card(
            color: Colors.white,
            surfaceTintColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 28.0, horizontal: 10),
              child: Wrap(
                spacing: 10,
                runSpacing: 20,
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CustomTextField(
                          borderColor: Colors.grey.shade900,
                          filledColor: Colors.transparent,
                          hint: 'Your Name',
                          label: 'Name',
                        ),
                        SizedBox(height: 10),
                        CustomTextField(
                          borderColor: Colors.grey.shade900,
                          filledColor: Colors.transparent,
                          hint: 'Email Address',
                          label: 'Email',
                        ),
                        SizedBox(height: 10),
                        CustomTextField(
                          borderColor: Colors.grey.shade900,
                          filledColor: Colors.transparent,
                          hint: 'Enter Phone Number',
                          label: 'Phone',
                        ),
                      ],
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.3),
                    child: CustomTextField(
                      borderColor: Colors.grey.shade900,
                      filledColor: Colors.transparent,
                      hint: 'Type a message here...',
                      label: 'Message ',
                      maxLines: 5,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Align(
                    child: TextButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.purple.shade700,
                          foregroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7),
                          )),
                      child: const Text('Send Message'),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
//12
//14
//15
//16