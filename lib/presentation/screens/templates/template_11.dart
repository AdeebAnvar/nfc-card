import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Template11 extends StatelessWidget {
  const Template11({super.key, required this.datas});
  final BusinessDatas datas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 2.8,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 3.5,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(datas.userModels.company.coverImage),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ProfilePicCard(datas: datas, isRounded: true, borderColor: Colors.white),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.all(18),
            child: Column(
              children: [
                Text(
                  datas.userModels.name,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  datas.userModels.designation,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  datas.userModels.company.companyName,
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(datas.userModels.profileDescription),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: datas.userModels.company.socialMediaConnections
                      .map(
                        (e) => Container(
                          margin: EdgeInsets.all(8),
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 1,
                                blurRadius: 8,
                              )
                            ],
                            color: Colors.pink.shade500,
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                            child: Icon(e.icon, color: Colors.white),
                          ),
                        ),
                      )
                      .toList(),
                ),
                SizedBox(height: 20),
                Column(
                  children: datas.userModels.contactDetails
                      .map(
                        (e) => Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: Card(
                            color: Colors.white,
                            surfaceTintColor: Colors.white,
                            elevation: 8,
                            child: Container(
                              width: MediaQuery.sizeOf(context).width,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 78.0, vertical: 30),
                                child: Column(
                                  children: [
                                    Icon(e.icon, color: Colors.pink.shade700, size: 40),
                                    Text(
                                      e.label,
                                      style: TextStyle(fontWeight: FontWeight.w300),
                                    ),
                                    Text(
                                      e.content,
                                      style: TextStyle(fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ],
            ),
          ),
          Title(label: 'QR Code'),
          SizedBox(height: 20),
          Column(
            children: [
              ProfilePicCard(datas: datas, isRounded: true, borderColor: Colors.white),
              SizedBox(height: 10),
              Container(
                width: 120,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Image.network(height: 100, datas.userModels.company.qrCode),
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Title(
            label: 'Our Services',
            isLeft: false,
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              children: datas.services
                  .map(
                    (e) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 18),
                            height: 160,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                image: NetworkImage(e.image),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Text(e.name),
                          Text(
                            e.description,
                            maxLines: 4,
                          ),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          SizedBox(height: 20),
          Title(label: '  Gallery   ', isLeft: false),
          SizedBox(height: 20),
          FlutterCarousel.builder(
            itemCount: datas.galleries.length,
            options: CarouselOptions(
              height: 400.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: const CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        datas.galleries[i].image,
                      ),
                    ),
                  ),
                  height: 250,
                  margin: const EdgeInsets.all(20),
                ),
              );
            },
          ),
          SizedBox(height: 20),
          Title(label: 'Products'),
          SizedBox(height: 20),
          FlutterCarousel.builder(
            itemCount: datas.products.length,
            options: CarouselOptions(
              height: 340.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Card(
                  color: Colors.white,
                  surfaceTintColor: Colors.white,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      // borderRadius: BorderRadius.circular(10),
                    ),
                    // height: //,
                    width: 250,
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.network(
                          fit: BoxFit.cover,
                          height: 100,
                          width: 200,
                          datas.products[i].image,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                datas.products[i].name,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              SizedBox(height: 15),
                              Text(
                                datas.products[i].description,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(height: 15),
                              Text(
                                datas.products[i].price,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'View More Products',
              style: TextStyle(
                color: Colors.pink.shade600,
                decoration: TextDecoration.underline,
                decorationColor: Colors.pink.shade700,
              ),
            ),
          ),
          SizedBox(height: 40),
          Title(label: ' Testimonials  ', isLeft: false),
          SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Card(
              elevation: 4,
              color: Colors.white,
              surfaceTintColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  children: [
                    CircleAvatar(
                      radius: 30,
                      backgroundImage: NetworkImage(datas.testimonials[0].image),
                    ),
                    SizedBox(height: 10),
                    Text(
                      datas.testimonials[0].label.toString(),
                      style: TextStyle(
                        color: Colors.pink.shade700,
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      datas.testimonials[0].testimonial.toString(),
                      maxLines: 4,
                      style: TextStyle(),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
          Title(label: 'Business Hours', isLeft: false),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.company.businessHours
                .map(
                  (e) => Container(
                    margin: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                    height: 90,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 2,
                          spreadRadius: 1,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: ListTile(
                        subtitle: Text(
                          e.isClosed ? 'Closed' : '${e.timeFrom} - ${e.timeTo}',
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        title: Text(e.day),
                        leading: Container(
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.pink.shade800,
                          ),
                          child: Center(
                            child: Icon(
                              Icons.calendar_month,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 50),
          Title(label: 'Make an Appointment'),
          SizedBox(height: 20),
          Container(
            // height: 100,
            margin: EdgeInsets.all(15),
            padding: EdgeInsets.all(15),
            width: MediaQuery.sizeOf(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 4,
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Date'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DatePickerField(
                    suffixIconColor: Colors.transparent,
                    fillColor: Colors.blue.shade50,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                Text('Hour'),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(color: Colors.black26, spreadRadius: 4, blurRadius: 4),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Make an Appointment',
                      style: TextStyle(
                        color: Colors.pink.shade600,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 40),
          // Container(
          //   height: MediaQuery.sizeOf(context).height / 2.8,
          //   child: Stack(
          //     children: [
          //       Container(
          //         height: MediaQuery.sizeOf(context).height / 3.8,
          //         decoration: BoxDecoration(
          //           image: DecorationImage(
          //             fit: BoxFit.cover,
          //             image: NetworkImage(AppImages.coverImage),
          //           ),
          //         ),
          //       ),
          //       Align(
          //         alignment: Alignment.bottomCenter,
          //         child: Container(
          //           height: 150,
          //           width: 150,
          //           decoration: BoxDecoration(
          //             shape: BoxShape.circle,
          //             image: DecorationImage(
          //               image: NetworkImage(AppImages.profilePic),
          //             ),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // Padding(
          //   padding: EdgeInsets.all(18),
          //   child: Column(
          //     children: [
          //       Text(
          //         'CEO',
          //         style: TextStyle(
          //           fontWeight: FontWeight.w400,
          //           fontSize: 16,
          //         ),
          //       ),
          //       SizedBox(height: 30),
          //       Align(
          //         alignment: Alignment.topLeft,
          //         child: Text(
          //           'Just a dummy description\n',
          //           style: TextStyle(fontWeight: FontWeight.w400),
          //         ),
          //       ),
          //       SizedBox(height: 40),
          //     ],
          //   ),
          // ),
          // Align(
          //   alignment: Alignment.topLeft,
          //   child: Container(
          //     width: MediaQuery.sizeOf(context).width / 4,
          //     decoration: BoxDecoration(
          //         color: Colors.pink,
          //         borderRadius: BorderRadius.only(
          //           bottomRight: Radius.circular(30),
          //         )),
          //     padding: EdgeInsets.all(10),
          //     child: Center(
          //       child: Text(
          //         'QR Code',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontWeight: FontWeight.w800,
          //           fontSize: 18,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          // SizedBox(height: 20),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          //     CircleAvatar(
          //       backgroundImage: NetworkImage(AppImages.profilePic),
          //       radius: 50,
          //     ),
          //     SizedBox(width: 20),
          //     Container(
          //       width: 130,
          //       height: 130,
          //       padding: EdgeInsets.all(5),
          //       decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(10),
          //         border: Border.all(color: Colors.black, width: 2),
          //       ),
          //       child: Center(
          //         child: Image.network(AppImages.qrCode),
          //       ),
          //     )
          //   ],
          // ),
          // SizedBox(height: 50),
          // Align(
          //   alignment: Alignment.topRight,
          //   child: Container(
          //     width: MediaQuery.sizeOf(context).width / 4,
          //     decoration: BoxDecoration(
          //         color: Colors.pink,
          //         borderRadius: BorderRadius.only(
          //           bottomLeft: Radius.circular(30),
          //         )),
          //     padding: EdgeInsets.all(10),
          //     child: Center(
          //       child: Text(
          //         'Inquiries',
          //         style: TextStyle(
          //           color: Colors.white,
          //           fontWeight: FontWeight.w800,
          //           fontSize: 18,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          // SizedBox(height: 20),
          // Padding(
          //   padding: EdgeInsets.all(18),
          //   child: Column(
          //     children: [
          //       CustomTextField(
          //         hintStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         labelStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         hint: 'Your Name',
          //         label: 'Name',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hintStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         labelStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         hint: 'Email Address',
          //         label: 'Email',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hintStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         labelStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         hint: 'Enter Phone Number',
          //         label: 'Phone',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         hint: 'Type a message here...',
          //         hintStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         labelStyle: TextStyle(color: Colors.grey.shade600, fontWeight: FontWeight.w300),
          //         label: 'Message ',
          //         maxLines: 5,
          //       ),
          //       SizedBox(height: 10),
          //       TextButton(
          //         onPressed: () {},
          //         style: TextButton.styleFrom(
          //             backgroundColor: Colors.pink,
          //             foregroundColor: Colors.white,
          //             shape: RoundedRectangleBorder(
          //               borderRadius: BorderRadius.circular(10),
          //             )),
          //         child: Text('Send Message'),
          //       )
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}

class Title extends StatelessWidget {
  const Title({
    super.key,
    required this.label,
    this.isLeft = true,
  });
  final String label;
  final bool isLeft;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: isLeft ? MainAxisAlignment.start : MainAxisAlignment.end,
      children: [
        Container(
          height: 60,
          // width: 100,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.pink.shade700,
              borderRadius: BorderRadius.only(
                bottomRight: isLeft ? Radius.circular(38) : Radius.zero,
                bottomLeft: !isLeft ? Radius.circular(38) : Radius.zero,
              )),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
