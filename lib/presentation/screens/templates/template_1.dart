// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';

import 'package:flutter_application_1/main.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

import '../../../widgets/custom_textfield.dart';

class Template1 extends StatelessWidget {
  const Template1({
    Key? key,
    required this.datas,
  }) : super(key: key);
  final BusinessDatas datas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 3,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 3.9,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(datas.userModels.company.coverImage),
                    ),
                  ),
                ),
                Align(alignment: Alignment.bottomCenter, child: ProfilePicCard(datas: datas)),
              ],
            ),
          ),
          SizedBox(height: 10),
          Column(
            children: [
              Text(
                datas.userModels.name,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 5),
              Text(
                datas.userModels.designation,
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14, color: Colors.grey),
              ),
              Text(
                datas.userModels.company.companyName,
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
              ),
              SizedBox(height: 40),
              Text(
                datas.userModels.profileDescription,
              ),
              SizedBox(height: 20),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: datas.userModels.company.socialMediaConnections
                .map(
                  (e) => Padding(
                    padding: EdgeInsets.symmetric(horizontal: 17),
                    child: Icon(e.icon, size: 27),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.contactDetails
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 18.0),
                    child: Column(
                      children: [
                        Icon(e.icon, size: 30, color: Color.fromARGB(255, 18, 10, 137)),
                        SizedBox(height: 10),
                        Text(e.content),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
          Container(
            width: MediaQuery.sizeOf(context).width,
            color: Colors.blueGrey.shade200,
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 24),
            child: Container(
              width: 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blueAccent.shade700,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage('https://mye.pw/web/media/avatars/150-26.jpg'),
                        ),
                        // borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      height: 100,
                      child: Image.network(
                          'https://imgs.search.brave.com/gW3hG6LUoOEHDu7YI9Vt_TSng96W0lL6YQekJU4mtTQ/rs:fit:860:0:0/g:ce/aHR0cHM6Ly91c2Vy/LWltYWdlcy5naXRo/dWJ1c2VyY29udGVu/dC5jb20vMjM2OTAx/NDUvODMzNDg3Mzkt/YzRmODhkMDAtYTM2/MS0xMWVhLTkzMmUt/ZTcyMmUwYmQxYjY1/LnBuZw'),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: MediaQuery.sizeOf(context).width,
            color: Colors.blueGrey.shade100,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
            child: Column(
              children: [
                Text(
                  'Our Services',
                  style: TextStyle(
                    color: Colors.blue.shade800,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 20),
                Column(
                  children: datas.services
                      .map(
                        (e) => Padding(
                          padding: EdgeInsets.symmetric(horizontal: 13, vertical: 10),
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 13),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image.network(
                                    height: 160,
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    e.image,
                                  ),
                                  SizedBox(height: 17),
                                  Text(
                                    e.name,
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                  SizedBox(height: 14),
                                  Text(
                                    e.description,
                                    maxLines: 4,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Gallery',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.blue.shade900,
              ),
            ),
          ),
          SizedBox(height: 20),
          FlutterCarousel.builder(
            itemCount: datas.galleries.length,
            options: CarouselOptions(
              height: 400.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(milliseconds: 600),
              slideIndicator: const CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Card(
                  color: Colors.white,
                  surfaceTintColor: Colors.white,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      // borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          datas.galleries[i].image,
                        ),
                      ),
                    ),
                    height: 250,
                    width: 200,
                    margin: const EdgeInsets.all(20),
                  ),
                ),
              );
            },
          ),
          Align(
            child: Text(
              'Products',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.blue.shade900,
              ),
            ),
          ),
          FlutterCarousel.builder(
            itemCount: datas.products.length,
            options: CarouselOptions(
              height: 350.0,
              autoPlay: true,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              floatingIndicator: false,
              showIndicator: false,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              slideIndicator: const CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
            itemBuilder: (c, i, ri) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 4,
                      blurRadius: 5,
                    ),
                  ],
                ),
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      height: 170,
                      // margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.red,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(datas.products[i].image),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          datas.products[i].name,
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          datas.products[i].description,
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(datas.products[i].price)
                      ],
                    )
                  ],
                ),
              );
            },
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'View More Products',
              style: TextStyle(
                color: Colors.blue,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          SizedBox(height: 40),
          Align(
            child: Text(
              'Testimonials',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.blue.shade900,
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 360,
            margin: EdgeInsets.symmetric(horizontal: 70),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 300,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 4,
                          blurRadius: 5,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: 60),
                        Text(
                          datas.testimonials[0].label.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 18.0),
                          child: Text(
                            datas.testimonials[0].testimonial.toString(),
                            maxLines: 7,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: CircleAvatar(
                    radius: 50,
                    backgroundImage: NetworkImage(datas.testimonials[0].image),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Business Hours',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),
          SizedBox(height: 20),
          Column(
            children: datas.userModels.company.businessHours
                .map(
                  (e) => Container(
                    margin: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                    height: 90,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 2,
                          spreadRadius: 1,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: ListTile(
                        subtitle: Text(
                          e.isClosed ? 'Closed' : '${e.timeFrom} - ${e.timeTo}',
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        title: Text(e.day),
                        leading: Container(
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.blue.shade800,
                          ),
                          child: Center(
                            child: Icon(
                              Icons.calendar_month,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          SizedBox(height: 20),
          Align(
            child: Text(
              'Make an Appointment',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
            ),
          ),
          SizedBox(height: 20),
          Container(
            // height: 100,
            margin: EdgeInsets.all(15),
            padding: EdgeInsets.all(15),
            width: MediaQuery.sizeOf(context).width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 4,
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Date'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DatePickerField(
                    suffixIconColor: Colors.transparent,
                    fillColor: Colors.blue.shade50,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                Text('Hour'),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.blue.shade800,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(color: Colors.black26, spreadRadius: 2, blurRadius: 4),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Make an Appointment',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 100),
        ],
      ),
      // body: SingleChildScrollView(
      //   child: Column(
      //     children: [
      //       Container(
      //         height: 250,
      //         // color: Colors.red,
      //         child: Stack(
      //           children: [
      //             Column(
      //               children: [
      //                 Container(
      //                   decoration: BoxDecoration(
      //                     image: DecorationImage(
      //                       fit: BoxFit.cover,
      //                       image: NetworkImage('https://mye.pw/assets/images/default_cover_image.jpg'),
      //                     ),
      //                     // color: Colors.amber,
      //                   ),
      //                   height: 200,
      //                 ),
      //               ],
      //             ),
      //             Align(
      //               alignment: Alignment.bottomCenter,
      //               child: Container(
      //                 width: 100,
      //                 height: 100,
      //                 decoration: BoxDecoration(
      //                   shape: BoxShape.circle,
      //                   color: Colors.blueAccent.shade700,
      //                   image: DecorationImage(
      //                     fit: BoxFit.cover,
      //                     image: NetworkImage('https://mye.pw/web/media/avatars/150-26.jpg'),
      //                   ),
      //                   // borderRadius: BorderRadius.circular(10),
      //                 ),
      //               ),
      //             ),
      //           ],
      //         ),
      //       ),
      //       SizedBox(height: 30),
      //       Text('Akhil Sabu', style: TextStyle(fontWeight: FontWeight.w300, color: Colors.grey.shade600)),
      //       SizedBox(height: 25),
      //       Text(
      //         'Just a dummy description',
      //         style: TextStyle(fontWeight: FontWeight.w300),
      //       ),
      //       SizedBox(height: 35),
      //       Container(
      //         // height: 200,
      //         padding: EdgeInsets.all(18),
      //         width: MediaQuery.sizeOf(context).width,
      //         color: Colors.blue.shade100.withOpacity(0.3),
      //         child: Column(
      //           children: [
      //             Text(
      //               'QR Code',
      //               style: TextStyle(fontWeight: FontWeight.w700, color: Colors.blue.shade900, fontSize: 19),
      //             ),
      //             SizedBox(height: 15),
      //             Container(
      //               width: 200,
      //               decoration: BoxDecoration(
      //                 color: Colors.white,
      //                 borderRadius: BorderRadius.circular(10),
      //               ),
      //               child: Padding(
      //                 padding: const EdgeInsets.all(18.0),
      //                 child: Column(
      //                   children: [
      //                     Container(
      //                       width: 70,
      //                       height: 70,
      //                       decoration: BoxDecoration(
      //                         shape: BoxShape.circle,
      //                         color: Colors.blueAccent.shade700,
      //                         image: DecorationImage(
      //                           fit: BoxFit.cover,
      //                           image: NetworkImage('https://mye.pw/web/media/avatars/150-26.jpg'),
      //                         ),
      //                         // borderRadius: BorderRadius.circular(10),
      //                       ),
      //                     ),
      //                     SizedBox(height: 20),
      //                     SizedBox(
      //                       height: 100,
      //                       child: Image.network(
      //                           'https://imgs.search.brave.com/gW3hG6LUoOEHDu7YI9Vt_TSng96W0lL6YQekJU4mtTQ/rs:fit:860:0:0/g:ce/aHR0cHM6Ly91c2Vy/LWltYWdlcy5naXRo/dWJ1c2VyY29udGVu/dC5jb20vMjM2OTAx/NDUvODMzNDg3Mzkt/YzRmODhkMDAtYTM2/MS0xMWVhLTkzMmUt/ZTcyMmUwYmQxYjY1/LnBuZw'),
      //                     ),
      //                   ],
      //                 ),
      //               ),
      //             ),
      //           ],
      //         ),
      //       ),
      //       SizedBox(height: 5),
      //       Container(
      //         width: MediaQuery.sizeOf(context).width,
      //         color: Colors.blue.shade100.withOpacity(0.3),
      //         padding: EdgeInsets.all(18),
      //         child: Column(
      //           children: [
      //             Text(
      //               'Inquiries',
      //               style: TextStyle(fontWeight: FontWeight.w700, color: Colors.blue.shade900, fontSize: 23),
      //             ),
      //             SizedBox(height: 20),
      //             CustomTextField(
      //               hint: 'Your Name',
      //               label: 'Name',
      //               prefixIcon: Icon(Icons.person, color: Colors.black12),
      //             ),
      //             SizedBox(height: 20),
      //             CustomTextField(
      //               hint: 'Email Address',
      //               label: 'Email',
      //               prefixIcon: Icon(Icons.mail, color: Colors.black12),
      //             ),
      //             SizedBox(height: 20),
      //             CustomTextField(
      //               hint: 'Enter Phone Number',
      //               label: 'Phone',
      //               prefixIcon: Icon(Icons.phone, color: Colors.black12),
      //             ),
      //             SizedBox(height: 20),
      //             CustomTextField(
      //               hint: 'Type a message here...',
      //               label: 'Message ',
      //               maxLines: 5,
      //             ),
      //             SizedBox(height: 10),
      //             Align(
      //               alignment: Alignment.bottomRight,
      //               child: TextButton(
      //                 onPressed: () {},
      //                 style: TextButton.styleFrom(
      //                     backgroundColor: Colors.blue.shade800,
      //                     foregroundColor: Colors.white,
      //                     shape: RoundedRectangleBorder(
      //                       borderRadius: BorderRadius.circular(10),
      //                     )),
      //                 child: Text('Send Message'),
      //               ),
      //             )
      //           ],
      //         ),
      //       ),
      //       SizedBox(height: 30),
      //     ],
      //   ),
      // ),
    );
  }
}
