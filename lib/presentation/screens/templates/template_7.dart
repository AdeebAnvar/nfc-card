import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_5.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:intl/intl.dart';

class Template7 extends StatelessWidget {
  const Template7({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      body: ListView(
        children: [
          Container(
            // height: MediaQuery.sizeOf(context).height / 2.2,
            color: Colors.black,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 3.2,
                  width: MediaQuery.sizeOf(context).width,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(AppImages.coverImage),
                    ),
                  ),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Transform.translate(
                          offset: Offset(0, 20),
                          child: Container(
                            clipBehavior: Clip.hardEdge,
                            height: 80,
                            width: MediaQuery.sizeOf(context).width / 1.5,
                            decoration: BoxDecoration(
                              color: Colors.white,
                            ),
                            // margin: EdgeInsets.only(bottom: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Akhil Sabu',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 17,
                                  ),
                                ),
                                Text(
                                  'CEO',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  'Festa',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(-73, 55),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            height: 120,
                            width: 120,
                            margin: const EdgeInsets.only(left: 30),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(AppImages.profilePic),
                                ),
                                border: Border.all(color: Colors.white, width: 3),
                                shape: BoxShape.circle
                                // borderRadius: BorderRadius.circular(14),
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 80),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: AppSocialMediaLinks.socialMediaLinks
                      .map(
                        (e) => Icon(
                          e,
                          size: 25,
                          color: Colors.white,
                        ),
                      )
                      .toList(),
                ),
                SizedBox(height: 30),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '  Just a dummy description\n',
                    style: TextStyle(fontWeight: FontWeight.w400, color: Colors.brown.shade100),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(),
                      ),
                      onPressed: () {},
                      child: Text('Contact'),
                    ),
                    SizedBox(width: 30),
                    Expanded(
                      child: Container(
                        height: 5,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Image.network('https://mye.pw/assets/img/vcard16/contact-lawyer-img.png'),
              SizedBox(height: 15),
              Column(
                children: contactCards
                    .map(
                      (e) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
                        child: Card(
                          color: Colors.white,
                          surfaceTintColor: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Row(
                              children: [
                                Icon(
                                  e['icon'],
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e['label'],
                                      style: TextStyle(
                                        color: Colors.grey.shade600,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15,
                                      ),
                                    ),
                                    Text(
                                      e['content'],
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
              SizedBox(height: 15),
              Container(
                padding: EdgeInsets.all(5),
                color: Colors.black,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 5,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: 30),
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.black,
                            shape: RoundedRectangleBorder(),
                          ),
                          onPressed: () {},
                          child: Text('Make an Appointment'),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      'Date : ',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 10),
                    TextField(
                      controller: datePickerController,
                      onTap: () async {
                        DateTime? dateTime = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2050),
                        );

                        datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                          border: OutlineInputBorder(),
                          hintText: 'Pick a Date',
                          suffixIcon: Icon(
                            Icons.calendar_month,
                            color: Colors.white54,
                          ),
                          fillColor: Colors.white12,
                          filled: true,
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          )),
                    ),
                    SizedBox(height: 10),
                    Text(
                      'Hour : ',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 10),
                    Align(
                      child: TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.black,
                          shape: RoundedRectangleBorder(),
                        ),
                        child: Text(
                          'Make an Appointment',
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 5,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(width: 30),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(),
                    ),
                    onPressed: () {},
                    child: Text('QR Code'),
                  ),
                ],
              ),
              SizedBox(height: 100),
              Container(
                height: 200,
                width: 200,
                margin: EdgeInsets.symmetric(horizontal: 30),
                decoration: BoxDecoration(
                  color: Colors.white,
                  // borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 5,
                      spreadRadius: 1,
                      blurStyle: BlurStyle.outer,
                    ),
                  ],
                ),
                child: Stack(
                  children: [
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(top: 50),
                        // height: 100,
                        // width: 100,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            // color: Colors.black,
                            // border: Border.all(color: Colors.black),
                            // borderRadius: BorderRadius.circular(16),
                            ),
                        child: Image.network(AppImages.qrCode),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Transform.translate(
                        offset: Offset(-12, -49),
                        child: Container(
                          height: 100,
                          width: 100,
                          margin: const EdgeInsets.only(left: 30),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(AppImages.profilePic),
                              ),
                              boxShadow: [
                                // BoxShadow(
                                //   color: Colors.black38,
                                //   blurRadius: 5,
                                //   spreadRadius: 1,
                                // )
                              ],
                              border: Border.all(
                                color: Colors.black,
                                width: 3,
                              ),
                              shape: BoxShape.circle
                              // borderRadius: BorderRadius.circular(30),
                              ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 70),
              Row(
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(),
                    ),
                    onPressed: () {},
                    child: Text('Our Services'),
                  ),
                  SizedBox(width: 30),
                  Expanded(
                    child: Container(
                      height: 5,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              GridView.builder(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 2,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 380,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  mainAxisExtent: 260,
                ),
                itemBuilder: (c, i) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.network(
                          height: 90,
                          'https://mye.pw/assets/img/vcard17/testimonial-bg.png',
                        ),
                        Text(
                          'Test ',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                          maxLines: 5,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w400, color: Colors.grey.shade600),
                        )
                      ],
                    ),
                  );
                },
              ),
              SizedBox(height: 70),
              Container(
                padding: EdgeInsets.symmetric(vertical: 25, horizontal: 5),
                decoration: BoxDecoration(
                  color: Colors.black,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage('https://mye.pw/assets/img/vcard16/gallery-bg.png'),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 5,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: 30),
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.black,
                            shape: RoundedRectangleBorder(),
                          ),
                          onPressed: () {},
                          child: Text('Gallery'),
                        ),
                      ],
                    ),
                    SizedBox(height: 50),
                    Center(
                      child: Container(
                        height: 200,
                        width: 200,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          image: NetworkImage('https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                        )),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 70),
              Row(
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(),
                    ),
                    onPressed: () {},
                    child: Text('Products'),
                  ),
                  SizedBox(width: 30),
                  Expanded(
                    child: Container(
                      height: 5,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                height: 300,
                child: ListView.builder(
                  itemCount: 2,
                  itemBuilder: (c, i) {
                    return Container(
                      // height: 100,
                      width: 210,
                      margin: EdgeInsets.all(8),
                      child: Card(
                        elevation: 4,
                        color: Colors.white,
                        surfaceTintColor: Colors.white,
                        child: Column(
                          children: [
                            Container(
                              height: 140,
                              width: double.infinity,
                              child: Image.network(
                                fit: BoxFit.cover,
                                'https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                              ),
                            ),
                            SizedBox(height: 10),
                            Text('Test Product 2'),
                            SizedBox(height: 4),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has  when an''',
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Text(
                                    '2880.00',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  scrollDirection: Axis.horizontal,
                ),
              ),
              SizedBox(height: 20),
              Container(
                color: Colors.black,
                child: Column(
                  children: [
                    Row(
                      children: [
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.black,
                            shape: RoundedRectangleBorder(),
                          ),
                          onPressed: () {},
                          child: Text('Testimonials'),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: Container(
                            height: 5,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                    CircleAvatar(
                      radius: 60,
                      backgroundImage: NetworkImage(
                        'https://www.mye.pw/uploads/vcards/testimonials/44/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 38.0, vertical: 18),
                      child: Text(
                        '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has  when an''',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white54),
                      ),
                    ),
                    Text(
                      'Test Testimonial',
                      style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                    ),
                    SizedBox(height: 60),
                    Row(
                      children: [
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.black,
                            shape: RoundedRectangleBorder(),
                          ),
                          onPressed: () {},
                          child: Text('Business Hours'),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: Container(
                            height: 5,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    Container(
                      margin: EdgeInsets.all(18),
                      padding: EdgeInsets.all(18),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.white),
                      ),
                      child: Column(
                        children: [
                          buildBusinessHours('Monday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Tuesday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Wednesday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Thursday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Friday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Saturday'),
                          SizedBox(height: 10),
                          buildBusinessHours('Sunday'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              //     SizedBox(height: 20),
              //     Row(
              //       children: [
              //         Expanded(
              //           child: Container(
              //             height: 5,
              //             color: Colors.black,
              //           ),
              //         ),
              //         SizedBox(width: 30),
              //         TextButton(
              //           style: TextButton.styleFrom(
              //             backgroundColor: Colors.black,
              //             foregroundColor: Colors.white,
              //             shape: RoundedRectangleBorder(),
              //           ),
              //           onPressed: () {},
              //           child: Text('Inquiries'),
              //         ),
              //       ],
              //     ),
              //   ],
              // ),
              // Padding(
              //   padding: const EdgeInsets.all(10.0),
              //   child: Wrap(
              //     spacing: 10,
              //     children: [
              //       ConstrainedBox(
              //         constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2),
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //           children: [
              //             CustomTextField(
              //               hint: 'Your Name',
              //               label: 'Name',
              //             ),
              //             SizedBox(height: 10),
              //             CustomTextField(
              //               hint: 'Email Address',
              //               label: 'Email',
              //             ),
              //             SizedBox(height: 10),
              //             CustomTextField(
              //               hint: 'Enter Phone Number',
              //               label: 'Phone',
              //             ),
              //           ],
              //         ),
              //       ),
              //       ConstrainedBox(
              //         constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.5),
              //         child: CustomTextField(
              //           hint: 'Type a message here...',
              //           label: 'Message ',
              //           maxLines: 5,
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // SizedBox(height: 20),
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 38.0),
              //   child: TextButton(
              //     onPressed: () {},
              //     style: TextButton.styleFrom(
              //         backgroundColor: Colors.black,
              //         foregroundColor: Colors.white,
              //         shape: RoundedRectangleBorder(
              //           borderRadius: BorderRadius.circular(2),
              //         )),
              //     child: Text('Send Message'),
              //   ),
              // ),
              // SizedBox(height: 100),
            ],
          ),
        ],
      ),
    );
  }

  Row buildBusinessHours(String title) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        Spacer(),
        Text(
          title == 'Sunday' || title == 'Saturday' ? 'Closed' : '09:00 AM - 09:00 PM',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
