import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/widgets/button.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Template10 extends StatelessWidget {
  const Template10({super.key, required this.datas});
  final BusinessDatas datas;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black.withAlpha(230),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              height: MediaQuery.sizeOf(context).height / 2.3,
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.sizeOf(context).height / 3,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(datas.userModels.company.coverImage),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 2,
                    left: 15,
                    child: ProfilePicCard(
                      datas: datas,
                      isRounded: false,
                      borderColor: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    datas.userModels.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    datas.userModels.designation,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    datas.userModels.company.companyName,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Divider(
                    color: Colors.greenAccent.shade700,
                    endIndent: 260,
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: datas.userModels.company.socialMediaConnections
                        .map(
                          (e) => Icon(
                            e.icon,
                            size: 27,
                            color: Colors.white,
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: 10),
                  Text(
                    datas.userModels.profileDescription,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 15),
                  Title(title: 'Contact'),
                  SizedBox(height: 15),
                  Column(
                    children: datas.userModels.contactDetails
                        .map(
                          (e) => ListTile(
                            leading: Container(
                              height: 40,
                              width: 40,
                              color: Colors.black54,
                              child: Center(
                                child: Icon(
                                  e.icon,
                                  color: Colors.green.shade700,
                                ),
                              ),
                            ),
                            title: Text(
                              e.label,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 13,
                              ),
                            ),
                            subtitle: Text(
                              e.content,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: 20),
                  Title(
                    title: 'Qr Code',
                    isLeftLineVisible: false,
                  ),
                  SizedBox(height: 26),
                  Align(
                    child: ProfilePicCard(
                      datas: datas,
                      isRounded: true,
                      borderColor: Colors.transparent,
                    ),
                  ),
                  SizedBox(height: 30),
                  Align(
                    child: Container(
                      height: 200,
                      width: 200,
                      color: Colors.black,
                      padding: EdgeInsets.all(24),
                      child: Image.network(datas.userModels.company.qrCode),
                    ),
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Our Services'),
                  SizedBox(height: 20),
                  Column(
                    children: datas.services
                        .map(
                          (e) => Container(
                            margin: EdgeInsets.all(13),
                            padding: EdgeInsets.all(13),
                            height: 250,
                            color: Colors.black,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.network(height: 100, e.image),
                                Text(
                                  e.name,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  e.description,
                                  maxLines: 4,
                                  style: TextStyle(
                                    color: Colors.white54,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Gallery'),
                  SizedBox(height: 20),
                  FlutterCarousel.builder(
                    itemCount: datas.galleries.length,
                    options: CarouselOptions(
                      height: 300.0,
                      autoPlay: true,
                      autoPlayCurve: Curves.easeInOut,
                      indicatorMargin: 10,
                      floatingIndicator: false,
                      showIndicator: true,
                      autoPlayAnimationDuration: const Duration(seconds: 1),
                      slideIndicator: CircularSlideIndicator(
                        indicatorBackgroundColor: Colors.black,
                        currentIndicatorColor: Colors.green.shade900,
                      ),
                    ),
                    itemBuilder: (c, i, ri) {
                      return Align(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            // borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                datas.galleries[i].image,
                              ),
                            ),
                          ),
                          height: 250,
                          margin: const EdgeInsets.all(20),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Products', isLeftLineVisible: false),
                  SizedBox(height: 20),
                  FlutterCarousel.builder(
                    itemCount: datas.products.length,
                    options: CarouselOptions(
                      height: 400.0,
                      autoPlay: true,
                      autoPlayCurve: Curves.easeInOut,
                      indicatorMargin: 10,
                      floatingIndicator: false,
                      showIndicator: true,
                      autoPlayAnimationDuration: const Duration(seconds: 1),
                      slideIndicator: CircularSlideIndicator(
                        indicatorBackgroundColor: Colors.black,
                        currentIndicatorColor: Colors.green.shade900,
                      ),
                    ),
                    itemBuilder: (c, i, ri) {
                      return Align(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.black,
                            // borderRadius: BorderRadius.circular(10),
                          ),
                          // height: //,
                          width: 200,
                          margin: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Image.network(
                                fit: BoxFit.contain,
                                // height: 100,
                                width: 200,
                                datas.products[i].image,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      datas.products[i].name,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(height: 15),
                                    Text(
                                      datas.products[i].description,
                                      style: TextStyle(
                                        color: Colors.white54,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                      ),
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(height: 15),
                                    Text(
                                      datas.products[i].price,
                                      style: TextStyle(
                                        color: Colors.green.shade800,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w900,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20),
                  Align(
                    child: Text(
                      'View More Products',
                      style: TextStyle(
                        color: Colors.greenAccent.shade700,
                        decoration: TextDecoration.underline,
                        decorationColor: Colors.greenAccent.shade700,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Testimonials'),
                  SizedBox(height: 20),
                  Container(
                    color: Colors.black,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Text(
                          datas.testimonials[0].testimonial,
                          maxLines: 3,
                          style: TextStyle(
                            color: Colors.white54,
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(height: 20),
                        ListTile(
                          leading: CircleAvatar(
                            backgroundImage: NetworkImage(datas.testimonials[0].image),
                            radius: 40,
                          ),
                          title: Text(
                            datas.testimonials[0].label.toString(),
                            style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w800),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Business Hours'),
                  Container(
                    color: Colors.black,
                    padding: EdgeInsets.all(20),
                    child: Column(
                        children: datas.userModels.company.businessHours
                            .map(
                              (e) => Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      e.day,
                                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                                    ),
                                    if (e.isClosed)
                                      Text(
                                        'Closed',
                                        style: TextStyle(color: e.isClosed ? Colors.greenAccent.shade700 : Colors.white, fontWeight: FontWeight.w500),
                                      )
                                    else
                                      Text(
                                        '${e.timeFrom} - ${e.timeTo}',
                                        style: TextStyle(color: e.isClosed ? Colors.greenAccent.shade700 : Colors.white, fontWeight: FontWeight.w500),
                                      ),
                                  ],
                                ),
                              ),
                            )
                            .toList()),
                  ),
                  SizedBox(height: 20),
                  Title(title: 'Make an Appointment', isLeftLineVisible: false),
                  SizedBox(height: 20),
                  Text(
                    'Date',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 10),
                  DatePickerField(
                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white10)),
                    fillColor: Colors.transparent,
                    hintTextColor: Colors.white60,
                    suffixIconColor: Colors.greenAccent.shade700,
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Hour',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 10),
                  Align(
                    child: Button(
                      backgroundColor: Colors.green.shade700,
                      foregroundColor: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 100),
            // Container(
            //   height: MediaQuery.sizeOf(context).height / 3,
            //   alignment: Alignment.bottomLeft,
            //   decoration: BoxDecoration(
            //     image: DecorationImage(
            //       fit: BoxFit.cover,
            //       image: NetworkImage(AppImages.coverImage),
            //     ),
            //   ),
            //   child: Row(
            //     children: [
            //       Column(
            //         mainAxisAlignment: MainAxisAlignment.end,
            //         children: [
            //           Container(
            //             width: 120,
            //             height: 120,
            //             margin: const EdgeInsets.only(left: 30, bottom: 20),
            //             decoration: BoxDecoration(
            //                 border: Border.all(color: Colors.white, width: 2),
            //                 image: DecorationImage(
            //                   image: NetworkImage(AppImages.profilePic),
            //                 )),
            //           ),
            //           const SizedBox(height: 10),
            //         ],
            //       ),
            //       const SizedBox(width: 30),
            //       Column(
            //         crossAxisAlignment: CrossAxisAlignment.start,
            //         mainAxisAlignment: MainAxisAlignment.center,
            //         children: [
            //           const Text(
            //             'CEO',
            //             style: TextStyle(
            //               color: Colors.grey,
            //               fontWeight: FontWeight.w400,
            //               fontSize: 16,
            //             ),
            //           ),
            //           const SizedBox(height: 10),
            //           Container(
            //             height: 2,
            //             color: Colors.green,
            //             width: 150,
            //           )
            //         ],
            //       ),
            //     ],
            //   ),
            // ),
            // const SizedBox(height: 20),
            // Padding(
            //   padding: const EdgeInsets.all(18.0),
            //   child: Column(
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       const Text(
            //         'Just a dummy description\n',
            //         style: TextStyle(fontWeight: FontWeight.w400, color: Colors.grey),
            //       ),
            //       const Row(
            //         children: [
            //           Expanded(
            //             child: Divider(
            //               color: Colors.green,
            //               endIndent: 30,
            //             ),
            //           ),
            //           Text(
            //             'Contact',
            //             style: TextStyle(
            //               fontWeight: FontWeight.w600,
            //               fontSize: 20,
            //               color: Colors.white,
            //             ),
            //           ),
            //           Expanded(
            //             child: Divider(
            //               color: Colors.green,
            //               indent: 30,
            //             ),
            //           ),
            //         ],
            //       ),
            //       const SizedBox(height: 20),
            //       Image.network('https://mye.pw/assets/img/vcard12/Email%20Icon.png'),
            //       const SizedBox(height: 30),
            //       Row(
            //         crossAxisAlignment: CrossAxisAlignment.start,
            //         children: [
            //           Column(
            //             crossAxisAlignment: CrossAxisAlignment.start,
            //             mainAxisAlignment: MainAxisAlignment.center,
            //             children: [
            //               const Text(
            //                 'QR Code',
            //                 style: TextStyle(
            //                   fontWeight: FontWeight.w600,
            //                   fontSize: 20,
            //                   color: Colors.white,
            //                 ),
            //               ),
            //               const SizedBox(height: 20),
            //               Container(
            //                 width: 120,
            //                 height: 120,
            //                 decoration: BoxDecoration(
            //                     border: Border.all(color: Colors.blueGrey.shade900, width: 5),
            //                     shape: BoxShape.circle,
            //                     image: DecorationImage(
            //                       image: NetworkImage(AppImages.profilePic),
            //                     )),
            //               ),
            //             ],
            //           ),
            //           const Expanded(
            //             flex: 3,
            //             child: Divider(
            //               color: Colors.green,
            //             ),
            //           ),
            //           const Spacer(),
            //           Container(
            //             height: 140,
            //             width: 130,
            //             decoration: BoxDecoration(
            //               border: Border.all(color: Colors.black, width: 10),
            //               image: DecorationImage(
            //                 image: NetworkImage(AppImages.qrCode),
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //       const SizedBox(height: 20),
            //       const Row(
            //         children: [
            //           Expanded(
            //             child: Divider(
            //               color: Colors.green,
            //               endIndent: 30,
            //             ),
            //           ),
            //           Text(
            //             'Inquiries',
            //             style: TextStyle(
            //               fontWeight: FontWeight.w600,
            //               fontSize: 20,
            //               color: Colors.white,
            //             ),
            //           ),
            //           Expanded(
            //             child: Divider(
            //               color: Colors.green,
            //               indent: 30,
            //             ),
            //           ),
            //         ],
            //       ),
            //       const SizedBox(height: 20),
            //       Wrap(
            //         spacing: 10,
            //         runSpacing: 20,
            //         children: [
            //           ConstrainedBox(
            //             constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.3),
            //             child: Column(
            //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //               children: [
            //                 CustomTextField(
            //                   borderColor: Colors.grey.shade900,
            //                   hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   filledColor: Colors.transparent,
            //                   hint: 'Your Name',
            //                   label: 'Name',
            //                 ),
            //                 SizedBox(height: 10),
            //                 CustomTextField(
            //                   borderColor: Colors.grey.shade900,
            //                   hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   filledColor: Colors.transparent,
            //                   hint: 'Email Address',
            //                   label: 'Email',
            //                 ),
            //                 SizedBox(height: 10),
            //                 CustomTextField(
            //                   borderColor: Colors.grey.shade900,
            //                   hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //                   filledColor: Colors.transparent,
            //                   hint: 'Enter Phone Number',
            //                   label: 'Phone',
            //                 ),
            //               ],
            //             ),
            //           ),
            //           ConstrainedBox(
            //             constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.3),
            //             child: CustomTextField(
            //               borderColor: Colors.grey.shade900,
            //               hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //               labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            //               filledColor: Colors.transparent,
            //               hint: 'Type a message here...',
            //               label: 'Message ',
            //               maxLines: 5,
            //             ),
            //           ),
            //         ],
            //       ),
            //       const SizedBox(height: 20),
            //       Align(
            //         child: TextButton(
            //           onPressed: () {},
            //           style: TextButton.styleFrom(
            //               backgroundColor: Colors.green.shade200,
            //               foregroundColor: Colors.white,
            //               shape: RoundedRectangleBorder(
            //                 borderRadius: BorderRadius.circular(7),
            //               )),
            //           child: const Text('Send Message'),
            //         ),
            //       ),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}

class Title extends StatelessWidget {
  const Title({
    super.key,
    required this.title,
    this.isLeftLineVisible = true,
  });
  final String title;
  final bool isLeftLineVisible;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        if (isLeftLineVisible)
          Expanded(
            child: Divider(color: Colors.greenAccent.shade700, indent: 20),
          ),
        Expanded(
          child: Text(
            '   $title',
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Expanded(
          child: Divider(
            color: Colors.greenAccent.shade700,
            endIndent: 20,
          ),
        ),
      ],
    );
  }
}
