import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';
import 'package:flutter_application_1/widgets/button.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:flutter_application_1/widgets/datepicker_field.dart';
import 'package:flutter_application_1/widgets/rounded_profpic.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Template9 extends StatelessWidget {
  const Template9({super.key, required this.datas});
  final BusinessDatas datas;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 2.7,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).height / 3.4,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        datas.userModels.company.coverImage,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ProfilePicCard(
                    datas: datas,
                    isRounded: true,
                    borderColor: Colors.blue.shade900,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 30),
          Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    datas.userModels.name,
                    style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 18),
                  ),
                  Text(datas.userModels.designation),
                  Text(datas.userModels.company.companyName),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 13.0, vertical: 44),
            child: Text(datas.userModels.profileDescription),
          ),
          Container(
            height: 70,
            color: Colors.grey.shade200,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: datas.userModels.company.socialMediaConnections
                  .map(
                    (e) => Icon(
                      e.icon,
                      size: 27,
                    ),
                  )
                  .toList(),
            ),
          ),
          const SizedBox(height: 10),
          Column(
            children: datas.userModels.contactDetails
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2),
                    child: Card(
                      color: const Color.fromARGB(255, 11, 39, 76),
                      child: ListTile(
                        leading: Icon(e.icon, color: Colors.white),
                        title: Text(
                          e.label,
                          style: const TextStyle(color: Colors.white),
                        ),
                        subtitle: Text(
                          e.content,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          const SizedBox(height: 30),
          Align(
            child: Text(
              'QR Code',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w600,
                color: Colors.blueGrey.shade800,
              ),
            ),
          ),
          const SizedBox(height: 60),
          Container(
            height: 200,
            margin: const EdgeInsets.symmetric(horizontal: 30),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                const BoxShadow(
                  color: Colors.black26,
                  blurRadius: 5,
                  spreadRadius: 1,
                  blurStyle: BlurStyle.outer,
                ),
              ],
            ),
            child: Stack(
              children: [
                Align(
                  child: Container(
                    margin: const EdgeInsets.only(left: 20, top: 50, bottom: 30, right: 20),
                    // height: 100,
                    // width: 100,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      // color: Colors.black,
                      // border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Image.network(datas.userModels.company.qrCode),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Transform.translate(
                    offset: const Offset(0, -49),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(datas.userModels.avatar),
                        ),
                        boxShadow: [
                          const BoxShadow(
                            color: Colors.black26,
                            blurRadius: 5,
                            spreadRadius: 1,
                          )
                        ],
                        border: Border.all(
                          color: Colors.blueGrey.shade800,
                          width: 3,
                        ),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          Align(
            child: Text(
              'Our Services',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w600,
                color: Colors.blueGrey.shade800,
              ),
            ),
          ),

          const SizedBox(height: 20),
          ListView(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: datas.services
                .map(
                  (e) => Card(
                    color: const Color.fromARGB(255, 11, 39, 76),
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Column(
                        children: [
                          CircleAvatar(
                            radius: 60,
                            backgroundColor: Colors.white,
                            backgroundImage: NetworkImage(e.image),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            e.name,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          const SizedBox(height: 15),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 10),
                            child: Text(
                              e.description,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
                .toList(),
          ),

          const SizedBox(height: 15),
          const Align(
            child: Text(
              'Gallery',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 15),
          Container(
            height: 360,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: const Color.fromARGB(255, 11, 39, 76),
                    height: 130,
                    margin: const EdgeInsets.only(bottom: 30),
                  ),
                ),
                FlutterCarousel.builder(
                  itemCount: datas.galleries.length,
                  options: CarouselOptions(
                    height: 400.0,
                    autoPlay: true,
                    autoPlayCurve: Curves.easeInOut,
                    indicatorMargin: 10,
                    floatingIndicator: false,
                    showIndicator: true,
                    autoPlayAnimationDuration: const Duration(seconds: 1),
                    slideIndicator: const CircularSlideIndicator(
                      indicatorBackgroundColor: Colors.grey,
                      currentIndicatorColor: Color.fromARGB(255, 11, 39, 76),
                    ),
                  ),
                  itemBuilder: (c, i, ri) {
                    return Align(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              datas.galleries[i].image,
                            ),
                          ),
                        ),
                        height: 250,
                        margin: const EdgeInsets.all(20),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
          const SizedBox(height: 40),
          const Align(
            child: Text(
              'Products',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 15),
          FlutterCarousel.builder(
            itemCount: datas.products.length,
            options: CarouselOptions(
              height: 450.0,
              autoPlay: true,
              showIndicator: false,
              autoPlayCurve: Curves.easeInOut,
              indicatorMargin: 10,
              autoPlayAnimationDuration: const Duration(seconds: 1),
            ),
            itemBuilder: (c, i, ri) {
              return Align(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      const BoxShadow(
                        color: Colors.black38,
                        blurRadius: 8,
                        spreadRadius: 3,
                      ),
                    ],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  // height: 250,
                  margin: const EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.network(
                        height: 190,
                        width: 200,
                        datas.products[i].image,
                      ),
                      Text(
                        datas.products[i].name,
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        datas.products[i].description,
                        maxLines: 3,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.grey.shade500,
                        ),
                      ),
                      Text(
                        datas.products[i].price,
                        style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 18),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
          const SizedBox(height: 15),
          const Align(
            child: Text(
              'View More Products',
              style: TextStyle(
                color: Color.fromARGB(255, 11, 39, 76),
                decoration: TextDecoration.underline,
                decorationColor: Color.fromARGB(255, 11, 39, 76),
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Align(
            child: Text(
              'Testimonials',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 25),
          Container(
            padding: EdgeInsets.all(18),
            height: MediaQuery.sizeOf(context).width / 1.8,
            margin: EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 11, 39, 76),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CircleAvatar(
                  radius: 45,
                  backgroundImage: NetworkImage(datas.testimonials[0].image),
                ),
                Text(
                  datas.testimonials[0].testimonial,
                  maxLines: 3,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w800,
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 25),
          const Align(
            child: Text(
              'Business Hours',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 15),
          Container(
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 11, 39, 76),
              borderRadius: BorderRadius.circular(15),
            ),
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.all(18),
            child: Column(
              children: datas.userModels.company.businessHours
                  .map(
                    (e) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            e.day,
                            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                          ),
                          if (e.isClosed)
                            Text(
                              'Closed',
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                            )
                          else
                            Text(
                              '${e.timeFrom} - ${e.timeTo}',
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                            ),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          const SizedBox(height: 15),
          const Align(
            child: Text(
              'Make an Appointment',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 15),
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.all(18),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 10,
                  spreadRadius: 3,
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(' Date'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DatePickerField(
                    suffixIconColor: Colors.transparent,
                    fillColor: Colors.blue.shade50.withOpacity(0.9),
                    border: InputBorder.none,
                  ),
                ),
                Text(' Hour'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Button(
                    size: Size(MediaQuery.sizeOf(context).width, 30),
                    backgroundColor: Color.fromARGB(255, 38, 38, 38),
                    foregroundColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          const Align(
            child: Text(
              'Inquiries',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            color: Colors.grey.shade100,
            padding: EdgeInsets.all(18),
            child: Column(
              children: [
                CustomTextField(
                  filledColor: Colors.grey.shade100,
                  hint: 'Your Name',
                  label: 'Name',
                ),
                SizedBox(height: 20),
                CustomTextField(
                  filledColor: Colors.grey.shade100,
                  hint: 'Email Address',
                  label: 'Email',
                ),
                SizedBox(height: 20),
                CustomTextField(
                  filledColor: Colors.grey.shade100,
                  hint: 'Enter Phone Number',
                  label: 'Phone',
                ),
                SizedBox(height: 20),
                CustomTextField(
                  filledColor: Colors.grey.shade100,
                  hint: 'Type a message here...',
                  label: 'Message ',
                  maxLines: 5,
                ),
                SizedBox(height: 20),
                TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blueGrey.shade900,
                    foregroundColor: Colors.white,
                    shape: RoundedRectangleBorder(),
                  ),
                  child: Text(
                    'Send Message',
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 100),
          // Container(
          //   height: MediaQuery.sizeOf(context).height / 3,
          //   alignment: Alignment.bottomRight,
          //   decoration: BoxDecoration(
          //     image: DecorationImage(
          //       fit: BoxFit.cover,
          //       image: NetworkImage(AppImages.coverImage),
          //     ),
          //   ),
          //   child: Transform.translate(
          //     offset: Offset(0, 50),
          //     child: Container(
          //       width: 120,
          //       height: 120,
          //       margin: EdgeInsets.only(right: 30),
          //       decoration: BoxDecoration(
          //           border: Border.all(color: Colors.blueGrey.shade900, width: 5),
          //           shape: BoxShape.circle,
          //           image: DecorationImage(
          //             image: NetworkImage(AppImages.profilePic),
          //           )),
          //     ),
          //   ),
          // ),
          // SizedBox(height: 50),
          // Padding(
          //   padding: EdgeInsets.all(18),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       Text(
          //         'CEO',
          //         style: TextStyle(
          //           fontWeight: FontWeight.w400,
          //           fontSize: 16,
          //         ),
          //       ),
          //       SizedBox(height: 40),
          //       Text(
          //         'Just a dummy description\n',
          //         style: TextStyle(fontWeight: FontWeight.w400),
          //       ),
          //       SizedBox(height: 20),
          //       Align(
          //         child: Text(
          //           'QR Code',
          //           style: TextStyle(
          //             fontSize: 24,
          //             fontWeight: FontWeight.w600,
          //             color: Colors.blueGrey.shade800,
          //           ),
          //         ),
          //       ),
          //       SizedBox(height: 60),
          //       Container(
          //         height: 200,
          //         margin: EdgeInsets.symmetric(horizontal: 30),
          //         decoration: BoxDecoration(
          //           color: Colors.white,
          //           borderRadius: BorderRadius.circular(10),
          //           boxShadow: [
          //             BoxShadow(
          //               color: Colors.black26,
          //               blurRadius: 5,
          //               spreadRadius: 1,
          //               blurStyle: BlurStyle.outer,
          //             ),
          //           ],
          //         ),
          //         child: Stack(
          //           children: [
          //             Align(
          //               child: Container(
          //                 margin: EdgeInsets.only(left: 20, top: 50, bottom: 30, right: 20),
          //                 // height: 100,
          //                 // width: 100,
          //                 padding: EdgeInsets.all(10),
          //                 decoration: BoxDecoration(
          //                   // color: Colors.black,
          //                   // border: Border.all(color: Colors.black),
          //                   borderRadius: BorderRadius.circular(16),
          //                 ),
          //                 child: Image.network(AppImages.qrCode),
          //               ),
          //             ),
          //             Align(
          //               alignment: Alignment.topCenter,
          //               child: Transform.translate(
          //                 offset: Offset(0, -49),
          //                 child: Container(
          //                   height: 100,
          //                   width: 100,
          //                   decoration: BoxDecoration(
          //                       image: DecorationImage(
          //                         image: NetworkImage(AppImages.profilePic),
          //                       ),
          //                       boxShadow: [
          //                         BoxShadow(
          //                           color: Colors.black26,
          //                           blurRadius: 5,
          //                           spreadRadius: 1,
          //                         )
          //                       ],
          //                       border: Border.all(
          //                         color: Colors.blueGrey.shade800,
          //                         width: 3,
          //                       ),
          //                       shape: BoxShape.circle
          //                       // borderRadius: BorderRadius.circular(30),
          //                       ),
          //                 ),
          //               ),
          //             ),
          //           ],
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // Container(
          //   color: Colors.grey.shade100,
          //   padding: EdgeInsets.all(18),
          //   child: Column(
          //     children: [
          //       CustomTextField(
          //         filledColor: Colors.grey.shade100,
          //         hint: 'Your Name',
          //         label: 'Name',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         filledColor: Colors.grey.shade100,
          //         hint: 'Email Address',
          //         label: 'Email',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         filledColor: Colors.grey.shade100,
          //         hint: 'Enter Phone Number',
          //         label: 'Phone',
          //       ),
          //       SizedBox(height: 20),
          //       CustomTextField(
          //         filledColor: Colors.grey.shade100,
          //         hint: 'Type a message here...',
          //         label: 'Message ',
          //         maxLines: 5,
          //       ),
          //       SizedBox(height: 20),
          //       TextButton(
          //         onPressed: () {},
          //         style: TextButton.styleFrom(
          //           backgroundColor: Colors.blueGrey.shade900,
          //           foregroundColor: Colors.white,
          //           shape: RoundedRectangleBorder(),
          //         ),
          //         child: Text(
          //           'Send Message',
          //         ),
          //       )
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}
