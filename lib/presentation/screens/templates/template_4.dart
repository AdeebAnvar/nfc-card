import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:intl/intl.dart';

class Template4 extends StatelessWidget {
  const Template4({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.black87,
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 2.6,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                opacity: 0.6,
                image: NetworkImage(
                  AppImages.coverImage,
                ),
              ),
            ),
            alignment: Alignment.bottomLeft,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Transform.translate(
                  offset: const Offset(0, 50),
                  child: Container(
                    height: 140,
                    width: 140,
                    margin: const EdgeInsets.only(left: 30),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(AppImages.profilePic),
                      ),
                      border: Border.all(
                        color: Colors.white,
                        width: 3,
                      ),
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Akhil Sabu',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      Text(
                        'CEO',
                        style: TextStyle(color: Colors.amber, fontWeight: FontWeight.w400, fontSize: 13),
                      ),
                      Text(
                        'Festa',
                        style: TextStyle(color: Colors.amber, fontWeight: FontWeight.w400, fontSize: 13),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 90),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: AppSocialMediaLinks.socialMediaLinks
                      .map(
                        (e) => Container(
                          width: 50,
                          height: 50,
                          margin: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: Colors.black87,
                            borderRadius: BorderRadius.circular(7),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white,
                              ),
                            ],
                          ),
                          child: Center(
                            child: Icon(
                              e,
                              size: 30,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Just a dummy description',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Contact',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 10),
                GridView.builder(
                  shrinkWrap: true,
                  itemCount: 6,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 340,
                    mainAxisExtent: 120,
                    crossAxisSpacing: 4,
                    mainAxisSpacing: 16,
                  ),
                  itemBuilder: (c, i) {
                    return Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              color: Colors.white12,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              contactCards[i]['icon'],
                              color: Colors.amber,
                            ),
                          ),
                          Text(
                            contactCards[i]['label'],
                            style: TextStyle(color: Colors.white60, fontWeight: FontWeight.w400),
                          ),
                          Text(
                            contactCards[i]['content'],
                            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 40),
                Text(
                  'Gallery',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 160),
                Text(
                  'Our Services',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 20),
                GridView.builder(
                  itemCount: 3,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 344,
                    mainAxisExtent: 230,
                  ),
                  itemBuilder: (c, i) {
                    return Card(
                      color: Colors.white12,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.network(
                              height: 50,
                              width: 50,
                              'https://www.mye.pw/uploads/vcards/products/42/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                            ),
                            Text(
                              'Test Service',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an''',
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.white54,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(height: 20),
                Text(
                  'Testimonials',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 40),
                Stack(
                  children: [
                    Image.network(
                      'https://mye.pw/assets/img/vcard20/testimonial-bg.png',
                      fit: BoxFit.cover,
                    ),
                    Align(
                      child: Container(
                        width: MediaQuery.sizeOf(context).width,
                        // height: 100,
                        margin: EdgeInsets.all(19),
                        padding: EdgeInsets.all(19),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.amber,
                            width: 3,
                          ),
                        ),
                        child: Stack(
                          children: [
                            Column(
                              children: [
                                SizedBox(height: 20),
                                Text(
                                  'Test Testimonial',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text(
                                  '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has  when an''',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                            Align(
                              child: Transform.translate(
                                offset: Offset(0, -60),
                                child: Container(
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      image: DecorationImage(
                                        image: Image.network(
                                          fit: BoxFit.contain,
                                          'https://www.mye.pw/uploads/vcards/testimonials/44/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                                        ).image,
                                      ),
                                      border: Border.all(
                                        color: Colors.amber,
                                        width: 2,
                                      )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text(
                  'Products',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 20),
                GridView.builder(
                  itemCount: 2,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 330,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 8,
                    mainAxisExtent: 280,
                  ),
                  itemBuilder: (c, i) {
                    return Container(
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              padding: EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white24,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SizedBox(height: 30),
                                  Text(
                                    'Test Product',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    'Lorem ipsum dolor sit AMET , print and guinj kilar kopil afik jatrq',
                                    style: TextStyle(
                                      color: Colors.white60,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '\$2,500.00',
                                    style: TextStyle(
                                      color: Colors.amber.shade800,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: CircleAvatar(
                              radius: 60,
                              backgroundImage: NetworkImage(
                                'https://www.mye.pw/uploads/vcards/products/42/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 20),
                Align(
                  child: Text(
                    'View More Products',
                    style: TextStyle(
                      color: Colors.amber.shade500,
                      fontWeight: FontWeight.w400,
                      decorationColor: Colors.amber.shade500,
                      fontSize: 14,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Text(
                  'Make an Appointment',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Text(
                      'Date : ',
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14, color: Colors.white),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: TextField(
                        controller: datePickerController,
                        onTap: () async {
                          DateTime? dateTime = await showDatePicker(
                            context: context,
                            firstDate: DateTime(2000),
                            lastDate: DateTime(2050),
                          );

                          datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                        },
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                            border: OutlineInputBorder(),
                            hintText: 'Pick a Date',
                            suffixIcon: Icon(
                              Icons.calendar_month,
                              color: Colors.amber.shade600,
                            ),
                            fillColor: Colors.white12,
                            filled: true,
                            hintStyle: TextStyle(color: Colors.white),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            )),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    Text(
                      'Hour : ',
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14, color: Colors.white),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                        child: TextButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        backgroundColor: const Color.fromARGB(255, 170, 149, 85),
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                          bottomLeft: Radius.circular(16),
                        )),
                      ),
                      child: Text(
                        'Make an Appointment',
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 40),
                Text(
                  'Business Hours',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 30),
                buildBusinessHours('Monday'),
                SizedBox(height: 10),
                buildBusinessHours('Tuesday'),
                SizedBox(height: 10),
                buildBusinessHours('Wednesday'),
                SizedBox(height: 10),
                buildBusinessHours('Thursday'),
                SizedBox(height: 10),
                buildBusinessHours('Friday'),
                SizedBox(height: 10),
                buildBusinessHours('Saturday'),
                SizedBox(height: 10),
                buildBusinessHours('Sunday'),
                SizedBox(height: 60),
                Text(
                  'QR Code',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.amber,
                  height: 2,
                  width: 20,
                ),
                SizedBox(height: 20),
                Container(
                  height: 100,
                  width: 100,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.network(AppImages.qrCode),
                ),
                // SizedBox(height: 20),
                // Text(
                //   'Inquiries',
                //   style: TextStyle(
                //     fontWeight: FontWeight.w600,
                //     color: Colors.white,
                //     fontSize: 20,
                //   ),
                // ),
                // Container(
                //   color: Colors.amber,
                //   height: 2,
                //   width: 20,
                // ),
                // SizedBox(height: 20),
                // Wrap(
                //   runSpacing: 20,
                //   spacing: 20,
                //   children: [
                //     ConstrainedBox(
                //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.49),
                //       child: TextField(
                //         style: TextStyle(color: Colors.white),
                //         decoration: InputDecoration(
                //           focusColor: Colors.amber,
                //           hintText: 'Your Name',
                //           hintStyle: TextStyle(color: Colors.white),
                //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //           enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //           focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //           border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //         ),
                //       ),
                //     ),
                //     ConstrainedBox(
                //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.49),
                //       child: TextField(
                //         style: TextStyle(color: Colors.white),
                //         decoration: InputDecoration(
                //           focusColor: Colors.amber,
                //           hintText: 'Email Address',
                //           hintStyle: TextStyle(color: Colors.white),
                //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //           enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //           focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //           border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //         ),
                //       ),
                //     ),
                //   ],
                // ),
                // SizedBox(height: 15),
                // TextField(
                //   style: TextStyle(color: Colors.white),
                //   decoration: InputDecoration(
                //     focusColor: Colors.amber,
                //     hintText: 'Enter Phone Number',
                //     hintStyle: TextStyle(color: Colors.white),
                //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //     enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //     focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //     border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //   ),
                // ),
                // SizedBox(height: 15),
                // TextField(
                //   style: TextStyle(color: Colors.white),
                //   decoration: InputDecoration(
                //     focusColor: Colors.amber,
                //     hintText: 'Type a message here',
                //     hintStyle: TextStyle(color: Colors.white),
                //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //     enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //     focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //     border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
                //   ),
                // ),
                // SizedBox(height: 24),
                // TextButton(
                //   onPressed: () {},
                //   style: TextButton.styleFrom(
                //     backgroundColor: const Color.fromARGB(255, 170, 149, 85),
                //     foregroundColor: Colors.white,
                //     shape: RoundedRectangleBorder(
                //         borderRadius: BorderRadius.only(
                //       topLeft: Radius.circular(16),
                //       topRight: Radius.circular(16),
                //       bottomLeft: Radius.circular(16),
                //     )),
                //   ),
                //   child: Text(
                //     'Send Message',
                //   ),
                // )
              ],
            ),
          ),
          SizedBox(height: 60),
        ],
      ),
    );
  }

  Row buildBusinessHours(String title) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        Spacer(),
        Text(
          title == 'Sunday' || title == 'Saturday' ? 'Closed' : '09:00 AM - 09:00 PM',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
