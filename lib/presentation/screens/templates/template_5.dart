import 'dart:ui';

import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:intl/intl.dart';

class Template5 extends StatelessWidget {
  const Template5({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: MediaQuery.sizeOf(context).height / 3.4,
            decoration: BoxDecoration(
              color: Colors.amber,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  AppImages.coverImage,
                ),
              ),
            ),
          ),
          SizedBox(height: 30),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Transform.translate(
                offset: Offset(0, -70),
                child: Container(
                  height: 140,
                  width: 140,
                  margin: const EdgeInsets.only(left: 30),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(AppImages.profilePic),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 5,
                        spreadRadius: 1,
                      )
                    ],
                    border: Border.all(
                      color: Colors.white,
                      width: 3,
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              SizedBox(width: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Akhil Sabu',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.brown.shade800,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  Text(
                    'CEO',
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13, color: Colors.brown),
                  ),
                  Text(
                    'Festa',
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13, color: Colors.brown),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Just a dummy description',
                    style: TextStyle(fontWeight: FontWeight.w400),
                  ),
                ),
                SizedBox(height: 20),
                GridView.builder(
                  primary: false,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    // crossAxisCount: 2,
                    maxCrossAxisExtent: 360,
                    // childAspectRatio: 10,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    mainAxisExtent: 100,
                  ),
                  itemCount: contactCards.length,
                  itemBuilder: (c, i) {
                    return Card(
                      color: Colors.brown.shade50,
                      surfaceTintColor: Colors.brown.shade50,
                      // elevation: 4,
                      child: Center(
                        child: ListTile(
                          title: Text(
                            contactCards[i]['content'],
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.brown.shade800,
                            ),
                          ),
                          subtitle: Text(
                            contactCards[i]['label'],
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.brown.shade800,
                            ),
                          ),
                          leading: Icon(
                            contactCards[i]['icon'],
                            color: Colors.brown.shade800,
                          ),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: AppSocialMediaLinks.socialMediaLinks
                      .map(
                        (e) => Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Center(
                            child: Icon(
                              e,
                              color: Colors.brown.shade800,
                              size: 35,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                SizedBox(height: 40),
                Text(
                  'Gallery',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 20),
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            'https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                          ))),
                ),
                SizedBox(height: 40),
                Text(
                  'Products',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 40),
                GridView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 350,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 280,
                  ),
                  itemBuilder: (c, i) {
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.brown.shade200,
                        ),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 100,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                  'https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 30),
                          Text(
                            'Test Product',
                            style: TextStyle(
                              color: Colors.brown,
                              fontWeight: FontWeight.w700,
                              fontSize: 14,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Lorem ipsum dolor sit amet fik printing cks fighuyu tiksaty res',
                            style: TextStyle(
                              color: Colors.grey.shade600,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          SizedBox(height: 10),
                          Align(
                            child: ActionChip(
                              onPressed: () {},
                              label: Text(
                                '\$2500.00',
                                style: TextStyle(
                                  color: Colors.brown.shade800,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 20),
                Align(
                  child: Text(
                    'View More Products',
                    style: TextStyle(
                      color: Colors.brown.shade800,
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      decorationColor: Colors.brown.shade800,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Text(
                  'Our Services',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 20),
                GridView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 350,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 40,
                  ),
                  itemBuilder: (c, i) {
                    return Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.brown),
                      ),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Transform.translate(
                            offset: Offset(0, -30),
                            child: Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.white, width: 5),
                              ),
                              child: Image.network(fit: BoxFit.cover, 'https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                            ),
                          ),
                          Text(
                            'Test Product',
                            style: TextStyle(
                              color: Colors.brown,
                              fontWeight: FontWeight.w700,
                              fontSize: 14,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Lorem ipsum dolor sit amet fik printing cks fighuyu tiksaty res',
                            style: TextStyle(
                              color: Colors.grey.shade600,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 4,
                          ),
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 30),
                Text(
                  'Make an Appointment',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 180,
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Date',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 13,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: datePickerController,
                  onTap: () async {
                    DateTime? dateTime = await showDatePicker(
                      context: context,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2050),
                    );

                    datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      border: OutlineInputBorder(),
                      hintText: 'Pick a Date',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
                SizedBox(height: 15),
                SizedBox(height: 5),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Hour',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 13,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {},
                  child: Text('Make an Appointment'),
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.brown.shade700,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(),
                      fixedSize: Size(MediaQuery.sizeOf(context).width, 40)),
                ),
                SizedBox(height: 50),
                Text(
                  'Testimonials',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 20),
                Container(
                  height: 100,
                  width: 100,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        'https://www.mye.pw/uploads/vcards/testimonials/44/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18.0),
                  child: Text(
                    '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit 
                    anim id est laborum.''',
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'Test Testimonial',
                  style: TextStyle(
                    color: Colors.brown.shade700,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 20),
                SizedBox(height: 40),
                Text(
                  'QR Code',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 100,
                      width: 100,
                      margin: const EdgeInsets.only(left: 30),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(AppImages.profilePic),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black38,
                            blurRadius: 5,
                            spreadRadius: 1,
                          )
                        ],
                        border: Border.all(
                          color: Colors.white,
                          width: 3,
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    SizedBox(width: 20),
                    Container(
                      height: 100,
                      width: 100,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        // color: Colors.black,
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Image.network(AppImages.qrCode),
                    ),
                  ],
                ),
                SizedBox(height: 50),
                Text(
                  'Business Hours',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
                ),
                Container(
                  color: Colors.brown,
                  height: 2,
                  width: 40,
                ),
                SizedBox(height: 20),

                Container(
                  margin: EdgeInsets.all(18),
                  padding: EdgeInsets.all(18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.brown.shade500),
                  ),
                  child: Column(
                    children: [
                      buildBusinessHours('Monday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Tuesday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Wednesday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Thursday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Friday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Saturday'),
                      SizedBox(height: 10),
                      buildBusinessHours('Sunday'),
                    ],
                  ),
                )
                // Text(
                //   'Inquiries',
                //   style: TextStyle(
                //     fontWeight: FontWeight.w600,
                //     fontSize: 20,
                //   ),
                // ),
                // Container(
                //   color: Colors.brown,
                //   height: 2,
                //   width: 40,
                // ),
                // SizedBox(height: 20),
                // Wrap(
                //   runSpacing: 20,
                //   spacing: 20,
                //   children: [
                //     ConstrainedBox(
                //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.25),
                //       child: TextField(
                //         style: TextStyle(color: Colors.grey),
                //         decoration: InputDecoration(
                //           focusColor: Colors.grey,
                //           hintText: 'Your Name',
                //           hintStyle: TextStyle(color: Colors.grey),
                //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //           enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //           focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //           border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //         ),
                //       ),
                //     ),
                //     ConstrainedBox(
                //       constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.25),
                //       child: TextField(
                //         style: TextStyle(color: Colors.grey),
                //         decoration: InputDecoration(
                //           focusColor: Colors.grey,
                //           hintText: 'Email Address',
                //           hintStyle: TextStyle(color: Colors.grey),
                //           contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //           enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //           focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //           border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //         ),
                //       ),
                //     ),
                //   ],
                // ),
                // SizedBox(height: 15),
                // TextField(
                //   style: TextStyle(color: Colors.grey),
                //   decoration: InputDecoration(
                //     focusColor: Colors.grey,
                //     hintText: 'Enter Phone Number',
                //     hintStyle: TextStyle(color: Colors.grey),
                //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //     enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //     focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //     border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //   ),
                // ),
                // SizedBox(height: 15),
                // TextField(
                //   style: TextStyle(color: Colors.grey),
                //   decoration: InputDecoration(
                //     focusColor: Colors.grey,
                //     hintText: 'Type a message here',
                //     hintStyle: TextStyle(color: Colors.grey),
                //     contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //     enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //     focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //     border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                //   ),
                // ),
                // SizedBox(height: 24),
                // TextButton(
                //   onPressed: () {},
                //   style: TextButton.styleFrom(
                //     backgroundColor: Colors.brown,
                //     foregroundColor: Colors.white,
                //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                //   ),
                //   child: Text(
                //     'Send Message',
                //   ),
                // )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Row buildBusinessHours(String title) {
  return Row(
    children: [
      Text(
        title,
        style: TextStyle(
          color: Colors.brown.shade700,
          fontWeight: FontWeight.w600,
        ),
      ),
      Spacer(),
      Text(
        title == 'Sunday' || title == 'Saturday' ? 'Closed' : '09:00 AM - 09:00 PM',
        style: TextStyle(
          color: Colors.brown.shade700,
          fontWeight: FontWeight.w600,
        ),
      ),
    ],
  );
}
