import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:intl/intl.dart';

class Template3 extends StatelessWidget {
  const Template3({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.grey.shade50,
      body: ListView(
        children: [
          Container(
            color: Colors.purple.withOpacity(0.02),
            height: MediaQuery.sizeOf(context).height / 2.1,
            child: Stack(
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width / 1.3,
                  height: MediaQuery.sizeOf(context).height / 3,
                  margin: EdgeInsets.all(18),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.elliptical(120, 120),
                        bottomLeft: Radius.elliptical(120, 120),
                      ),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(AppImages.coverImage),
                      )),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                      padding: EdgeInsets.only(right: 10, bottom: 10),
                      child: Container(
                        height: 135,
                        width: 135,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 4),
                          image: DecorationImage(
                            image: NetworkImage(AppImages.profilePic),
                          ),
                        ),
                      )),
                ),
                Positioned(
                  bottom: 1,
                  left: 20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Akhil Sabu',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue.shade800,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      Text(
                        'CEO',
                        style: TextStyle(fontWeight: FontWeight.w400, fontSize: 13),
                      ),
                      Text(
                        'Festa',
                        style: TextStyle(fontWeight: FontWeight.w400, fontSize: 13),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SocialMediaLinks(),
                SizedBox(height: 25),
                Text(
                  '  Just a dummy description',
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),
                ),
                SizedBox(height: 70),
                GridView.builder(
                  primary: false,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    // crossAxisCount: 2,
                    maxCrossAxisExtent: 360,
                    // childAspectRatio: 10,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    mainAxisExtent: 100,
                  ),
                  itemCount: contactCards.length,
                  itemBuilder: (c, i) {
                    return Card(
                      color: Colors.white,
                      surfaceTintColor: Colors.white,
                      elevation: 4,
                      child: Center(
                        child: ListTile(
                          title: Text(
                            contactCards[i]['content'],
                            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                          subtitle: Text(
                            contactCards[i]['label'],
                            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14, color: Colors.grey.shade700),
                          ),
                          leading: Icon(
                            contactCards[i]['icon'],
                            color: Colors.blue.shade900,
                          ),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(height: 20),
                Image.network('https://mye.pw/assets/img/vcard21/blossom.png'),
                Align(
                  child: Text(
                    '#Our Services',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 16),
                GridView.builder(
                  primary: false,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    // crossAxisCount: 2,
                    maxCrossAxisExtent: 350,
                    // childAspectRatio: 10,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 240,
                  ),
                  itemCount: contactCards.length,
                  itemBuilder: (c, i) {
                    return Container(
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 1,
                                    blurRadius: 8,
                                  ),
                                ],
                              ),
                              height: MediaQuery.sizeOf(c).height / 4,
                              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 17),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Test',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    '''Lorem Ipsum is simply dummy text of the and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer''',
                                    style: TextStyle(
                                      overflow: TextOverflow.ellipsis,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13,
                                    ),
                                    maxLines: 4,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: 95,
                              height: 95,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.grey.shade50),
                              child: Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: Colors.deepPurple.shade800, width: 10),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: Image.network('https://www.mye.pw/uploads/vcards/products/42/leon-wu-LLfRMRT-9AY-unsplash.jpg').image,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 20),
                Align(
                  child: Text(
                    '#Make an Appointment',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: datePickerController,
                  onTap: () async {
                    DateTime? dateTime = await showDatePicker(
                      context: context,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2050),
                    );

                    datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      border: OutlineInputBorder(),
                      hintText: 'Pick a Date',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
                SizedBox(height: 15),
                TextButton(
                  onPressed: () {},
                  child: Text('Make an Appointment'),
                  style: TextButton.styleFrom(
                      backgroundColor: Colors.deepPurple.shade700,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(),
                      fixedSize: Size(MediaQuery.sizeOf(context).width, 40)),
                ),
                SizedBox(height: 20),
                Align(alignment: Alignment.topRight, child: Image.network('https://mye.pw/assets/img/vcard21/appointment-bg-img.png')),
                SizedBox(height: 10),
                Row(
                  children: [
                    SizedBox(width: 56),
                    Image.network('https://mye.pw/assets/img/vcard21/gallery-bg-img.png'),
                    SizedBox(width: 26),
                    Text(
                      '#Gallery',
                      style: TextStyle(
                        color: Color.fromARGB(255, 84, 56, 224),
                        fontWeight: FontWeight.w800,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                Image.network('https://www.mye.pw/uploads/vcards/gallery/45/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                SizedBox(height: 10),
                Image.network('https://mye.pw/assets/img/vcard21/testimonial-bg-img.png'),
                SizedBox(height: 10),
                Align(
                  child: Text(
                    '#Testimonials',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer,''',
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 15),
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    Container(
                      height: 70,
                      width: 60,
                      child: Image.network(
                        'https://www.mye.pw/uploads/vcards/testimonials/44/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                      ),
                    ),
                    SizedBox(width: 10),
                    Text(
                      'Test Testimonial',
                      style: TextStyle(
                        color: Colors.blue.shade900,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 20),
                Align(alignment: Alignment.topRight, child: Image.network('https://mye.pw/assets/img/vcard21/product-bg-img.png')),
                Align(
                  child: Text(
                    '#Products',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                GridView.builder(
                  primary: false,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    // crossAxisCount: 2,
                    maxCrossAxisExtent: 340,
                    // childAspectRatio: 10,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 280,
                  ),
                  itemCount: 2,
                  itemBuilder: (c, i) {
                    return Card(
                      color: Colors.white,
                      surfaceTintColor: Colors.white,
                      elevation: 4,
                      child: Column(
                        children: [
                          Container(
                            clipBehavior: Clip.hardEdge,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                              ),
                            ),
                            child: Image.network(
                              fit: BoxFit.cover,
                              'https://www.mye.pw/uploads/vcards/products/42/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                            ),
                          ),
                          SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text('Test Product'),
                                    Spacer(),
                                    Text(
                                      '\$25000',
                                      style: TextStyle(color: Colors.blue.shade900, fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10),
                                Text(
                                  '''Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer''',
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 10),
                Align(
                  child: Text(
                    'View More Products',
                    style: TextStyle(
                      color: Colors.blue.shade800,
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                SizedBox(height: 100),
                Align(alignment: Alignment.topRight, child: Image.network('https://mye.pw/assets/img/vcard21/hour-bg-img.png')),
                Align(
                  child: Text(
                    '#Business Hours',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Wrap(
                  // alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  // crossAxisAlignment: WrapCrossAlignment.center,
                  runSpacing: 10,
                  spacing: 10,
                  children: [
                    ActionChip(
                      label: Text(
                        'Monday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Tuesday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Wednesday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Thursday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Friday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Saturday :09:00 AM - 09:00 PM',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                    ActionChip(
                      label: Text(
                        'Sunday - Closed',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      onPressed: () {},
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.blue.shade600),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Image.network('https://mye.pw/assets/img/vcard21/qr-code-bg-img.png'),
                Align(
                  child: Text(
                    '#QR Code',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage(AppImages.profilePic),
                    ),
                    SizedBox(width: 35),
                    Image.network(
                      AppImages.qrCode,
                      height: 90,
                    ),
                  ],
                ),
                SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerRight,
                  child: Image.network('https://mye.pw/assets/img/vcard21/contact-us-bg-img.png'),
                ),
                Align(
                  child: Text(
                    '#Inquiries',
                    style: TextStyle(
                      color: Color.fromARGB(255, 84, 56, 224),
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
                SizedBox(height: 15),
                CustomTextField(
                  hint: 'Your Name',
                  label: 'Name',
                  // prefixIcon: Icon(Icons.person, color: Colors.black12),
                ),
                SizedBox(height: 20),
                CustomTextField(
                  hint: 'Email Address',
                  label: 'Email',
                  // prefixIcon: Icon(Icons.mail, color: Colors.black12),
                ),
                SizedBox(height: 20),
                CustomTextField(
                  hint: 'Enter Phone Number',
                  label: 'Phone',
                  // prefixIcon: Icon(Icons.phone, color: Colors.black12),
                ),
                SizedBox(height: 20),
                CustomTextField(
                  hint: 'Type a message here...',
                  label: 'Message ',
                  maxLines: 5,
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                        fixedSize: Size(200, 40),
                        backgroundColor: Colors.blue.shade800,
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                        )),
                    child: Text('Send Message'),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}

class SocialMediaLinks extends StatelessWidget {
  const SocialMediaLinks({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: AppSocialMediaLinks.socialMediaLinks
            .map(
              (e) => Card(
                color: Colors.white,
                surfaceTintColor: Colors.white,
                shape: CircleBorder(),
                elevation: 3,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Center(
                    child: Icon(
                      e,
                      color: Colors.blue.shade800,
                      size: 35,
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}

List<Map<String, dynamic>> contactCards = [
  {'label': 'Email', 'content': 'akhilsabuv@gmail.com', 'icon': Icons.mail},
  {'label': 'Alternate Email', 'content': 'akhilsabuv@gmail.com', 'icon': Icons.mail},
  {'label': 'Mobile Number', 'content': '+911234567890', 'icon': Icons.phone},
  {'label': 'Mobile Number', 'content': '+911234567890', 'icon': Icons.phone},
  {'label': 'Date Of Birth', 'content': '23/04/2001', 'icon': Icons.cake},
  {'label': 'Address', 'content': 'Lucas Street , Imayath Line', 'icon': Icons.location_on},
];
