import 'package:flutter/Material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/constants/images.dart';
import 'package:flutter_application_1/constants/links.dart';
import 'package:flutter_application_1/main.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_3.dart';
import 'package:flutter_application_1/presentation/screens/templates/template_5.dart';
import 'package:flutter_application_1/widgets/custom_container.dart';
import 'package:flutter_application_1/widgets/custom_textfield.dart';
import 'package:intl/intl.dart';

class Template2 extends StatelessWidget {
  const Template2({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return Scaffold(
      body: ListView(
        children: [
          ClipPath(
            clipper: CustomContainer(),
            child: Container(
              color: Colors.blue,
              height: 265,
              child: Column(
                children: [
                  ClipPath(
                    clipper: CustomContainer(),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.red,
                      ),
                      height: 250,
                      clipBehavior: Clip.antiAlias,
                      child: Image.network(fit: BoxFit.cover, AppImages.coverImage),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0, -80),
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Akhil Sabu',
                              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 17),
                            ),
                            Text('CEO', style: TextStyle(fontWeight: FontWeight.w300)),
                            Text('Festa', style: TextStyle(fontWeight: FontWeight.w300)),
                            SizedBox(height: 30),
                          ],
                        ),
                      ),
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.blueAccent.shade700,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(AppImages.profilePic),
                          ),
                          // borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: AppSocialMediaLinks.socialMediaLinks
                        .map(
                          (e) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Card(
                              color: Colors.white,
                              surfaceTintColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(7),
                              ),
                              elevation: 3,
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Center(
                                  child: Icon(
                                    e,
                                    color: Colors.blue.shade800,
                                    size: 35,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Just a dummy description'),
                      Image.network('https://mye.pw/assets/img/vcard18/earth.png'),
                    ],
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Contact',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 20),
                  GridView.builder(
                    primary: false,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      // crossAxisCount: 2,
                      maxCrossAxisExtent: 340,
                      // childAspectRatio: 10,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20,
                      mainAxisExtent: 100,
                    ),
                    itemCount: contactCards.length,
                    itemBuilder: (c, i) {
                      return Center(
                        child: ListTile(
                          title: Text(
                            contactCards[i]['content'],
                            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 13),
                          ),
                          subtitle: Text(
                            contactCards[i]['label'],
                            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 12, color: Colors.grey.shade700),
                          ),
                          leading: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: Colors.blue,
                                )),
                            child: Icon(
                              contactCards[i]['icon'],
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      'QR Code',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 50),
                  Container(
                    height: 200,
                    width: MediaQuery.sizeOf(context).width,
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.blue,
                        )),
                    child: Column(
                      children: [
                        Transform.translate(
                          offset: Offset(0, -40),
                          child: Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.blueAccent.shade700,
                              border: Border.all(color: Colors.blue, width: 2),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage('https://mye.pw/web/media/avatars/150-26.jpg'),
                              ),
                              // borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 80,
                          child: Image.network(AppImages.qrCode),
                        ),
                        SizedBox(height: 30)
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      'Our Services',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 10),
                  Card(
                    color: Colors.white,
                    surfaceTintColor: Colors.white,
                    child: ListTile(
                      contentPadding: EdgeInsets.all(10),
                      title: Text(
                        'Test Service 2',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                        ),
                      ),
                      subtitle: Text(
                        '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      leading: Image.network('https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                    ),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      'Gallery',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    height: 150,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                          ),
                        )),
                  ),
                  SizedBox(height: 40),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Products',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 10),
                  GridView.builder(
                    primary: false,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      // crossAxisCount: 2,
                      maxCrossAxisExtent: 348,
                      // childAspectRatio: 10,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 220,
                    ),
                    itemCount: 3,
                    itemBuilder: (c, i) {
                      return Card(
                        color: Colors.white,
                        surfaceTintColor: Colors.white,
                        elevation: 4,
                        child: Column(
                          children: [
                            Container(
                              height: 150,
                              // width: 200,
                              width: double.infinity,
                              child: Image.network(fit: BoxFit.cover, 'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg'),
                            ),
                            Padding(
                              padding: EdgeInsets.all(12),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Test Product',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          // fontSize: 14,
                                        ),
                                      ),
                                      Spacer(),
                                      Text(
                                        '\$2500.00',
                                        style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    'Lorem ipsum dolor sit amet income hijack sot y umi',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20),
                  Text(
                    'View More Products',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.blue,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(height: 40),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Testimonials',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 140,
                    width: 140,
                    decoration: BoxDecoration(color: Colors.blue, shape: BoxShape.circle),
                    padding: EdgeInsets.all(6),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 4),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            'https://www.mye.pw/uploads/vcards/services/46/leon-wu-LLfRMRT-9AY-unsplash.jpg',
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Test Testimonial',
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.''',
                      maxLines: 5,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 30),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Business Hours',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(18),
                    padding: EdgeInsets.all(18),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.blue),
                    ),
                    child: Column(
                      children: [
                        buildBusinessHours('Monday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Tuesday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Wednesday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Thursday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Friday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Saturday'),
                        SizedBox(height: 10),
                        buildBusinessHours('Sunday'),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Make an Appointment',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Date',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: datePickerController,
                    onTap: () async {
                      DateTime? dateTime = await showDatePicker(
                        context: context,
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2050),
                      );

                      datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
                    },
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        border: OutlineInputBorder(),
                        hintText: 'Pick a Date',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        )),
                  ),
                  SizedBox(height: 15),
                  SizedBox(height: 5),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Hour',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text('Make an Appointment'),
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.blue.shade700,
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(),
                        fixedSize: Size(MediaQuery.sizeOf(context).width, 40)),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      'Inquiries',
                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                  SizedBox(height: 20),
                  Wrap(
                    runSpacing: 20,
                    spacing: 20,
                    children: [
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.39),
                        child: CustomTextField(
                          hint: 'Your Name',
                          label: 'Name',
                          prefixIcon: Icon(Icons.person, color: Colors.blue),
                        ),
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: MediaQuery.sizeOf(context).width / 2.39),
                        child: CustomTextField(
                          hint: 'Email Address',
                          label: 'Email',
                          prefixIcon: Icon(Icons.mail, color: Colors.blue),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomTextField(
                    hint: 'Enter Phone Number',
                    label: 'Phone',
                    prefixIcon: Icon(Icons.phone, color: Colors.blue),
                  ),
                  SizedBox(height: 20),
                  CustomTextField(
                    hint: 'Type a message here...',
                    label: 'Message ',
                    maxLines: 5,
                  ),
                  SizedBox(height: 30),
                  TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                        foregroundColor: Colors.blue.shade800,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: Colors.blue.shade800,
                          ),
                          borderRadius: BorderRadius.circular(5),
                        )),
                    child: Text('Send Message'),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
