import 'package:flutter/Material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.label,
    required this.hint,
    this.prefixIcon,
    this.maxLines,
    this.filledColor,
    this.hintStyle,
    this.labelStyle,
    this.borderColor,
  });
  final String label;
  final String hint;
  final Widget? prefixIcon;
  final int? maxLines;
  final Color? filledColor;
  final TextStyle? hintStyle;
  final TextStyle? labelStyle;
  final Color? borderColor;
  @override
  Widget build(BuildContext context) {
    return TextField(
      maxLines: maxLines,
      decoration: InputDecoration(
        fillColor: filledColor ?? Colors.white,
        filled: true,
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        labelText: label,
        hintText: hint,
        hintStyle: hintStyle ?? TextStyle(color: Colors.grey.shade500, fontWeight: FontWeight.w400),
        labelStyle: labelStyle ?? TextStyle(color: Colors.blue.shade900, fontWeight: FontWeight.w400),
        prefixIcon: prefixIcon,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: borderColor ?? Colors.black12,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: borderColor ?? Colors.black12,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: borderColor ?? Colors.black12,
          ),
        ),
      ),
    );
  }
}
