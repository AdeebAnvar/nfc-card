import 'package:flutter/Material.dart';

class Button extends StatelessWidget {
  const Button({
    super.key,
    this.backgroundColor,
    this.foregroundColor,
    this.borderRadius,
    this.size,
  });
  final Color? backgroundColor;
  final Color? foregroundColor;
  final BorderRadiusGeometry? borderRadius;
  final Size? size;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
        fixedSize: size,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius ?? BorderRadius.zero,
        ),
      ),
      child: Text(
        'Make an Appointment',
      ),
    );
  }
}
