import 'package:flutter/Material.dart';
import 'package:intl/intl.dart';

class DatePickerField extends StatelessWidget {
  const DatePickerField({super.key, this.suffixIconColor, this.hintTextColor, this.fillColor, this.border});
  final Color? suffixIconColor;
  final Color? hintTextColor;
  final Color? fillColor;
  final InputBorder? border;
  @override
  Widget build(BuildContext context) {
    TextEditingController datePickerController = TextEditingController();
    return TextField(
      controller: datePickerController,
      onTap: () async {
        DateTime? dateTime = await showDatePicker(
          context: context,
          firstDate: DateTime(2000),
          lastDate: DateTime(2050),
        );

        datePickerController.text = DateFormat("MMM d, yyyy").format(dateTime ?? DateTime.now());
      },
      decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          border: border,
          hintText: 'Pick a Date',
          suffixIcon: Icon(
            Icons.calendar_month,
            color: suffixIconColor,
          ),
          fillColor: fillColor ?? Colors.white12,
          filled: true,
          hintStyle: TextStyle(color: hintTextColor),
          enabledBorder: border),
    );
  }
}
