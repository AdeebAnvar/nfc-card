import 'package:flutter/Material.dart';
import 'package:flutter_application_1/data/repositories/repositories.dart';

class ProfilePicCard extends StatelessWidget {
  const ProfilePicCard({
    super.key,
    required this.datas,
    this.isRounded = true,
    this.borderColor = Colors.white,
  });

  final BusinessDatas datas;
  final bool isRounded;
  final Color borderColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      height: 130,
      decoration: BoxDecoration(
        shape: isRounded ? BoxShape.circle : BoxShape.rectangle,
        border: Border.all(color: borderColor, width: 5),
        image: DecorationImage(
          image: NetworkImage(datas.userModels.avatar),
        ),
      ),
    );
  }
}
